<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="java.io.BufferedReader,java.io.ByteArrayOutputStream,java.io.IOException,java.io.InputStreamReader,java.io.OutputStreamWriter,java.net.*,java.util.logging.Level,java.util.logging.Logger,java.sql.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
<link href="css/reset-fonts-grids.css" type="text/css" rel="stylesheet"></link>
<link href="css/container.css" type="text/css" rel="stylesheet"></link>
<link href="css/style.css" type="text/css" rel="stylesheet"></link>
<link href="css/treeview-core.css" type="text/css" rel="stylesheet"></link>
<link href="css/treeview-skin.css" type="text/css" rel="stylesheet"></link>
<link href="css/menu-core.css" type="text/css" rel="stylesheet"></link>
<script type="text/javascript" src="lib/jquery-1.8.2.js"></script>
<title>Upload File</title>
<script type="text/javascript">
      function validateProcess() {

          var sFileName = document.getElementById("file").value;
          var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1];

          if (document.getElementById("file").value == "") {
              alert("Please fill attachment first.");
          }
          else if (sFileExtension != "xls") {
              alert("Extention type is .xls only");
          }
          else {
              document.fileuploadform.submit();
              $("#file").attr('disabled', 'true');
              $("#submitQueryForm").attr('disabled', 'true');
              $("#messageSubmit").css('display', 'block')
          }
      }

      function downloadTemplate(type) {
          var location = location.hostname + ":" + location.port + "<%=request.getContextPath()%>/DownloadServlet?type=" + type;
          alert(location);
          window.location("<%=request.getContextPath()%>/DownloadServlet?type=" + type);
	}
</script>
</head>
<body>
	<div id="pageContent"
		style="display: block; width: 100%; height: auto; vertical-align: top;">
		<table cellspacing="0" cellpadding="0" summary=""
			style="width: 100%; height: 100%;">
			<tbody>
				<tr height="12px"></tr>
				<tr>
					<td width="10px"></td>
					<td align="center" style="vertical-align: top">
						<!--<noscript>--> 
						<!-- only shown if scripting is disabled -->
						<!--  <h1>You currently have JavaScript disabled for this web site.
                   			   You must enable scripting in your browser for the content
                    		   server to function properly.</h1>
              			</noscript>-->
						<div class="mainContent">
							<form method="post" action="UploadServlet" name="fileuploadform"
								enctype="multipart/form-data" target="_parent">
								<div class="mainContent">
									<table 
										style="width: 400px; border-collapse: separate; border-spacing: 5px; ">
										<tbody>
											<tr>
												<td style="width: 35%; text-align: right;"><span
													class="searchLabel">Path File: </span></td>
												<td style="width: 65%; text-align: left;"><input
													type="file" name="file" id="file"></input>
												</td>
											</tr>
										</tbody>
									</table>
									<hr></hr>							
									<table 
										style="width: 400px; border-collapse: separate; border-spacing: 5px; ">
										<tbody>
											<tr>
												<td style="width: 35%; text-align: right;">&nbsp;</td>
												<td style="width: 65%; text-align: left;">	
														<input type="hidden" name="fSource" id="fSource" value="DIS">
														<input type="button" id="submitQueryForm" name="submitQueryForm" value="  Upload Data  " onclick="validateProcess()"></input>
														<input type="hidden" name="pheAp" value="<%=request.getParameter("ap")%>">
												</td>
											</tr>
										</tbody>
									</table>
									<br></br> 
									<span
										id="messageSubmit" style="display: none; color: red">
										<img src="images/Loading_Icon.gif" width="12"></img> Please wait.. Do not close your browser...
									</span>
								</div>
							</form>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</body>
</html>