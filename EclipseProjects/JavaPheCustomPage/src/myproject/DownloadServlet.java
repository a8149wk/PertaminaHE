package myproject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.DatabaseValue;

public class DownloadServlet
  extends HttpServlet
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
  private static final int BYTES_DOWNLOAD = 2048;
  
  public void init(ServletConfig config)
    throws ServletException
  {
    super.init(config);
  }
  
  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    response.setContentType("text/html; charset=windows-1252");
    
    String templateFile = getTemplateFile(request.getParameter("type"));
    File file = new File(templateFile);
    if (!templateFile.equals("none"))
    {
      response.setHeader("Content-Disposition", getServletContext().getMimeType(file.getName()));
      
      response.setHeader("Content-Length", String.valueOf(file.length()));
      
      response.setHeader("Content-Disposition", "attachment;filename=\"" + file.getName() + "\"");
      
      BufferedInputStream input = null;
      BufferedOutputStream output = null;
      try
      {
        input = new BufferedInputStream(new FileInputStream(file));
        output = new BufferedOutputStream(response.getOutputStream());
        
        byte[] buffer = new byte['?'];
        for (int length = 0; (length = input.read(buffer)) > 0;) {
          output.write(buffer, 0, length);
        }
      }
      finally
      {
        if (output != null) {
          try
          {
            output.close();
          }
          catch (IOException ignore) {}
        }
        if (input != null) {
          try
          {
            input.close();
          }
          catch (IOException ignore) {}
        }
      }
    }
  }
  
  public String getTemplateFile(String type)
  {
    DatabaseValue dValue = new DatabaseValue();
    String path = "";
    path = dValue.getPheConfigValue("LOCATION_TEMPLATE_FILE_PATH_TDC");
    if (path.length() > 0)
    {
      if (type.equals("TDCNonDrawing")) {
        return path + "Template TDC Master List Non Drawing.xls";
      }
      if (type.equals("TDCDrawing")) {
        return path + "Template TDC Master List Drawing.xls";
      }
      return "none";
    }
    return "none";
  }
}
