package myproject;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import util.DatabaseValue;

public class UploadServlet
  extends HttpServlet
{
  private boolean isMultipart;
  private String filePath;
  private int maxMemSize = 52428800;
  private File file;
  
  public void init()
  {
    this.filePath = getServletContext().getInitParameter("file-upload");
  }
  
  public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    String folderEDMSDestination = "";
    
    DatabaseValue dbValue = new DatabaseValue();
    
    this.isMultipart = ServletFileUpload.isMultipartContent(request);
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    if (!this.isMultipart)
    {
      out.println("<html>");
      out.println("<head>");
      out.println("<title>Servlet upload</title>");
      out.println("</head>");
      out.println("<body>");
      out.println("<p>No file uploaded</p>");
      out.println("</body>");
      out.println("</html>");
      return;
    }
    Date date = new Date();
    long dateLong = date.getTime();
    String xPath = String.valueOf(dateLong);
    
    DiskFileItemFactory factory = new DiskFileItemFactory();
    
    factory.setSizeThreshold(this.maxMemSize);
    
    String repositoryPath = dbValue.getPheConfigValue("REPOSITORY_PATH");
    factory.setRepository(new File(repositoryPath));
    
    ServletFileUpload upload = new ServletFileUpload(factory);
    
    int maxFileSize = 1024;
    int size = Integer.valueOf(dbValue.getPheConfigValue("UPLOAD_EXCEL_MAX_SIZE")).intValue();
    
    maxFileSize *= size;
    upload.setSizeMax(maxFileSize);
    
    String folderEDMSLocation = "";
    String fSource = "";
    //*** Start Added by Hadi Wijaya 24022019 ***//
    String pheAp = "";
    //*** End Added by Hadi Wijaya 24022019 ***//
    try
    {
      List<FileItem> fileItems = upload.parseRequest(request);
      
      Iterator i = fileItems.iterator();
      
      out.println("<html>");
      out.println("<head>");
      out.println("<title>Servlet upload</title>");
      out.println("</head>");
      out.println("<body>");
      for (FileItem fi : fileItems)
      {
        boolean checkFormField = fi.isFormField();
        if (checkFormField)
        {
          if (fi.getFieldName().equals("txtFolder")) {
            folderEDMSDestination = fi.getString();
          }
          if (fi.getFieldName().equals("fSource")) {
              fSource = fi.getString();
            }
          //*** Start Added by Hadi Wijaya 24022019 ***//
          if (fi.getFieldName().equals("pheAp")) {
        	  pheAp = fi.getString();
            }
          //*** End Added by Hadi Wijaya 24022019 ***//
          folderEDMSDestination = folderEDMSDestination.replace("/", "\\\\");
        }
        if (!checkFormField)
        {
          String fieldName = fi.getFieldName();
          
          String fileName = fi.getName();
          
          String contentType = fi.getContentType();
          boolean isInMemory = fi.isInMemory();
          long sizeInBytes = fi.getSize();
          if (fileName.lastIndexOf("\\") >= 0)
          {
            this.file = new File(this.filePath + xPath + fileName.substring(fileName.lastIndexOf("\\") + 1));
            
            xPath = xPath + fileName.substring(fileName.lastIndexOf("\\") + 1);
          }
          else
          {
            this.file = new File(this.filePath + xPath + fileName.substring(fileName.lastIndexOf("\\") + 1));
            
            xPath = xPath + fileName.substring(fileName.lastIndexOf("\\") + 1);
          }
          fi.write(this.file);
          
          out.println("Uploaded Filename: " + fileName + "<br>");
        }
      }
      out.println("</body>");
      out.println("</html>");
      
      DatabaseValue dValue = new DatabaseValue();
      String pathRedirect = dValue.getPheConfigValue("URL_UPLOAD_FILE_SUBMIT");
      
      String type = "";
      if (folderEDMSDestination.length() > 0) {
        type = "EDMS";
      } else if (fSource.equalsIgnoreCase("DIS")) {
        type = "DIS";
      } else if (request.getParameter("extresource") != "") {
        type = "EXTRESOURCE";
      } else {
        type = "TDC";
      }
      
      //*** Start Change by Hadi Wijaya 24022019 ***//
      String fullPath = "http://" + request.getServerName() + ":" + dValue.getPheConfigValue("PORT_URM") + pathRedirect + xPath + "&folderSource=" + folderEDMSDestination + "&type=" + type + "&fSource=" + fSource + "&pheAp=" + pheAp;
      //*** End Change by Hadi Wijaya 24022019 ***//
      /*
      System.out.println("-------------------------------------------------------");
      System.out.println("Server Name     : " + request.getServerName());
      System.out.println("URM Port        : " + dValue.getPheConfigValue("PORT_URM"));
      System.out.println("Path Redirect   : " + pathRedirect);
      System.out.println("XPath           : " + xPath);
      System.out.println("Folder EDMS Dest: " + folderEDMSDestination);
      System.out.println("FSource         : " + fSource);
      System.out.println("Type            : " + type);
      System.out.println("PHE AP Name     : " + pheAp);
      System.out.println("-------------------------------------------------------");
      */
      response.sendRedirect(fullPath);
    }
    catch (Exception ex)
    {
      System.out.println(ex);
    }
  }
  
  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    throw new ServletException("GET method used with " + getClass().getName() + ": POST method required.");
  }
}
