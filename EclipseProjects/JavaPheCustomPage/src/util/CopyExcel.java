package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class CopyExcel
{
  public void getXlsFile(String path)
  {
    try
    {
      FileInputStream file = new FileInputStream(new File(path));
      
      HSSFWorkbook workbook = new HSSFWorkbook(file);
      
      HSSFSheet sheet = workbook.getSheetAt(0);
      
      Iterator<Row> rowIterator = sheet.iterator();
      while (rowIterator.hasNext())
      {
        Row row = (Row)rowIterator.next();
        
        Iterator<Cell> cellIterator = row.cellIterator();
        while (cellIterator.hasNext())
        {
          Cell cell = (Cell)cellIterator.next();
          switch (cell.getCellType())
          {
          case 4: 
            System.out.print(cell.getBooleanCellValue() + "\t\t");
            break;
          case 0: 
            System.out.print(cell.getNumericCellValue() + "\t\t");
            break;
          case 1: 
            System.out.print(cell.getStringCellValue() + "\t\t");
          }
        }
        System.out.println("");
      }
      file.close();
      Date date = new Date();
      long dateLong = date.getTime();
      FileOutputStream out = new FileOutputStream(new File("D:\\temp" + dateLong + ".xls"));
      
      workbook.write(out);
      out.close();
    }
    catch (FileNotFoundException e)
    {
      e.printStackTrace();
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }
}
