package util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseValue
{
  public String getPheConfigValue(String keyconfig)
  {
    DatabaseConnection dbConnection = new DatabaseConnection();
    String keyvalue = "";
    Connection conn = null;
    Statement stmt = null;
    try
    {
      conn = dbConnection.getConnection();
      stmt = conn.createStatement();
      String query = "SELECT KEY_VALUE FROM PHE_CONFIG WHERE KEY_CONFIG = '" + keyconfig + "'";
      
      ResultSet rs = stmt.executeQuery(query);
      while (rs.next()) {
        keyvalue = rs.getString("KEY_VALUE");
      }
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    finally
    {
      try
      {
        stmt.close();
      }
      catch (SQLException ex) {}
      try
      {
        conn.close();
      }
      catch (SQLException ex) {}
    }
    return keyvalue;
  }
}
