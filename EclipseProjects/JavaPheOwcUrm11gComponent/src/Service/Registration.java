/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import ScheduledEvent.ScheduledEvt;
import data.DatabaseValue;
import intradoc.common.Log;
import intradoc.data.DataException;
import intradoc.data.Workspace;
import intradoc.server.Service;
import java.sql.SQLException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.DataCollection;
import util.PheMailWs;
import util.UserManagement;
import util.UserManagementWLS;

/**
 *
 * @author Ginanjar
 */
public class Registration extends Service {

    public void registerBusinessContact() {
//        Log.info("PHE:registerBusinessContact:username:" + m_binder.getLocal("username"));
//        Log.info("PHE:registerBusinessContact:email:" + m_binder.getLocal("email"));
//        Log.info("PHE:registerBusinessContact:password:" + m_binder.getLocal("password"));
        
        DatabaseValue dValue = new DatabaseValue();

        try {
            String id = "";
            try {
                id = dValue.getIDFullNameFromBusinessContact(m_binder.getLocal("username"));
                
//                Log.info("PHE:registerBusinessContact:id:" + id);
            } catch (DataException ex) {
                Logger.getLogger(Registration.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (id.length() > 0) {
                DataCollection dColl = new DataCollection();
                Workspace ws = dColl.getSystemWorkspace();
                //buat dapetin username baru dari hasil generate an
                String username = generateUsername(m_binder.getLocal("username"));
                
                //username yang udah dibentuk dimasukin ke variable externalUser buat dikirimin email nya..
                m_binder.putLocal("externalUser", username);
                
                //update data yang udah ada..
                boolean executeUpdate = dValue.updateUsernameBusinessContactByID(id,username,ws);
                
                if(true){
//                    uManagement.connect();
//                    uManagement.createUser(m_binder.getLocal("username"), m_binder.getLocal("password"), m_binder.getLocal("email"), m_binder.getLocal("username"), "Business Contact");
                    UserManagementWLS umwls = new UserManagementWLS();
                    umwls.createWeblogicUser(username, m_binder.getLocal("username"), m_binder.getLocal("password"), m_binder.getLocal("email"), "BusinessContact", "LOCAL");
                    
//                    Log.info("PHE:registerBusinessContact:createWebkigicUser:" );
                    String strMessage = "";
                    strMessage = strMessage + "<p>Dear Mr. / Mrs. <br><br>";
                    strMessage = strMessage + "You can access DSWF External Application at https://pheonwj.pertamina.com/dswf/ <br>";
                    strMessage = strMessage + "Detail of information will be listed below : <br><br>";
                    strMessage = strMessage + "username : " + username + "<br>";
                    strMessage = strMessage + "password : " +  m_binder.getLocal("password") + "<br><br></p>";
                    strMessage = strMessage + "<p>If you need any assistance how to revalidate, please contact Technical Document Control <br>"
                                            + "onwj.tdc@pheonwj.pertamina.com or dial 3132 for the extension number. </p>";
                    strMessage = strMessage + "<p>Thank you for your attention.<br></p>";
                    strMessage = strMessage + "<p><b>Technical Document Control</b><br></p>";
                   
                    Log.info("PHE:registerBusinessContact:beforeSendEmail:" + m_binder.getLocal("email") );
//                    PheMailWs mailWS = new PheMailWs();
//                    mailWS.callEmail(m_binder.getLocal("email"), "[PHEONWJ]New Business Contact", strMessage);

                    m_binder.putLocal("strMessage", strMessage);
                    m_binder.putLocal("strSubject", "[PHEONWJ]New Business Contact");
                    m_binder.putLocal("strTo", m_binder.getLocal("email"));
                    Log.info("PHE:registerBusinessContact:afterSendEmail" );
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(Registration.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String generateUsername(String fullName) {
        DatabaseValue dValue = new DatabaseValue();
        
        String separateFullName[] = fullName.split(" ");
        
//        Log.info("PHE:generateUsername:fullName" + fullName);
        String usernameTemp = "";
        for (int i = 0; i < separateFullName.length; i++) {
            if(i == 0){
                usernameTemp = usernameTemp + separateFullName[i];
            }
            else if (separateFullName[i].length() > 3) {
                usernameTemp = usernameTemp + separateFullName[i].substring(0, 3);
            } else {
                usernameTemp = usernameTemp+separateFullName[i];
            }
        }

        usernameTemp = usernameTemp.toLowerCase() + "";
        boolean isReGenerate = false;
        String username = "";
        do {
            Random randomGenerator = new Random();
            int randomInt = randomGenerator.nextInt(90) + 10;

            if (username.length() == 0) {
                username = String.valueOf(randomInt);
                username = usernameTemp + username;
            }
            
//            Log.info("PHE:generateUsername:insideDo:username:" + username);
            try {
                String usernameDB = dValue.getUsernameBusinessContactByUsername(username);
//                Log.info("PHE:generateUsername:usernameDB:" + usernameDB);
                if (usernameDB.length() > 0) {
                    try {
                        int lastNumber = 0;
                        lastNumber = Integer.valueOf(usernameDB.substring(usernameDB.indexOf("." + 1), usernameDB.length()));
                        username = username.substring(0, username.indexOf(".")) + String.valueOf(lastNumber);
                    } catch (Exception ex) {
                        Log.error("PHE:generateUsername:error:" + ex.getMessage());
                    }
                    isReGenerate = true;
                } else {
                    isReGenerate = false;
                }
            } catch (DataException ex) {
                Log.error("PHE:generateUsername:error2:" + ex.getMessage());
            }
        } while (isReGenerate);
        Log.info("PHE:generateUsername:username:" + username);
        return username;
    }

}
