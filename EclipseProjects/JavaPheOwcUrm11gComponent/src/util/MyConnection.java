/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Hashtable;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;

/**
 *
 * @author Ginanjar
 */
public class MyConnection {

    private static MBeanServerConnection connection = null;
    private static JMXConnector connector = null;
    private static final ObjectName service = null;
    /*
     * Initialize connection to the Domain Runtime MBean Server.
     */

    public static void initConnection(String hostname, String portString,
            String username, String password) throws IOException,
            MalformedURLException {
        String protocol = "t3";
        Integer portInteger = Integer.valueOf(portString);
        int port = portInteger.intValue();
        String jndiroot = "/jndi/";
        String mserver = "weblogic.management.mbeanservers.domainruntime";
        JMXServiceURL serviceURL = new JMXServiceURL(protocol, hostname, port,
                jndiroot + mserver);
        Hashtable h = new Hashtable();
        h.put(Context.SECURITY_PRINCIPAL, username);
        h.put(Context.SECURITY_CREDENTIALS, password);
        h.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES,
                "weblogic.management.remote");
        connector = JMXConnectorFactory.connect(serviceURL, h);
        connection = connector.getMBeanServerConnection();
    }

    public static void main(String[] args) throws Exception {
        new MyConnection();
        String hostname = "localhost";
        String portString = "7001";
        String username = "weblogic";
        String password = "welcome1";
        MyConnection c = new MyConnection();
        MonitorServlets s = new MonitorServlets();
        initConnection(hostname, portString, username, password);
        s.getServletData();
        connector.close();
    }

    public void printNameAndState() throws Exception {
        ObjectName[] serverRT = getServerRuntimes();
        System.out.println("got server runtimes");
        int length = (int) serverRT.length;
        for (int i = 0; i < length; i++) {
            String name = (String) connection.getAttribute(serverRT[i],
                    "Name");
            String state = (String) connection.getAttribute(serverRT[i],
                    "State");
            System.out.println("Server name: " + name + ".   Server state: "
                    + state);
        }
    }

    public static ObjectName[] getServerRuntimes() throws Exception {
        return (ObjectName[]) connection.getAttribute(service,
                "ServerRuntimes");
    }
}