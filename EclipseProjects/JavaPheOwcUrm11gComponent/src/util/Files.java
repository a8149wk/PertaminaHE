/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import data.DatabaseValue;
import java.io.File;

/**
 *
 * @author Ginanjar
 */
public class Files {

    public boolean deleteFile(String path) {
        try {
            DatabaseValue dValue = new DatabaseValue();
            File file = new File( path);

            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }

        } catch (Exception e) {

            e.printStackTrace();
            return false;
        }
        return true;
    }
}
