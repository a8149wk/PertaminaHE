/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import checkin.CheckinSimilar;
import data.DatabaseValue;

import intradoc.common.Log;
import intradoc.common.ServiceException;

import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.ResultSet;
import intradoc.data.Workspace;
import intradoc.server.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.DataCollection;
import util.Validator;
/**
 *
 * @author AkhmadH
 */
public class MultipleUpdate extends Service{
    
    public String executeUpdateDocInfoMultiple2(
            DataBinder binder,
            String dDocName,
            String dID,
            String metadataUpd,
            String xOldDocNumberData,
            String xAuthorData,
            String xAuthorOrganizationData,
            String xAuthorGroupNameData,
            String xProjectNameData,
            String xContractorData,
            String xContractNumberData,
            String xWONumberData,
            String xCommentsData,
            String xRetentionPeriodData,
            String xRetentionDateData,
            String dDocAccountData,
            String dSecurityGroupData,
            String xRemarksData,
            int countData,
            String userLogin)
            throws DataException, ServiceException {
        String returnFunction = "";
        String idcToken = "";
        
        DataCollection dColl = new DataCollection();
        DatabaseValue dValue = new DatabaseValue();
        DataBinder binders = new DataBinder();
        idcToken = this.m_binder.getLocal("idcToken");
        String arrDocName[] = dDocName.split(",");
        String arrID[] = dID.split(",");
        
        for(int i=0; i<countData; i++) {
                binders.putLocal("IdcService", "UPDATE_DOCINFO");
		binders.putLocal("dDocName", arrDocName[i]); //LATEST
                binders.putLocal("dID", arrID[i]);
                binders.putLocal("xOldDocNumber", xOldDocNumberData);
                binders.putLocal("xAuthor", xAuthorData);
                binders.putLocal("xAuthorOrganization", xAuthorOrganizationData);
                binders.putLocal("xAuthorGroupName", xAuthorGroupNameData);
                binders.putLocal("xProjectName", xProjectNameData);
                binders.putLocal("xContractor", xContractorData);
                binders.putLocal("xContractNumber", xContractNumberData);
                binders.putLocal("xWONumber", xWONumberData);
                binders.putLocal("xComments", xCommentsData);
                binders.putLocal("xRetentionPeriod", xRetentionPeriodData);
                binders.putLocal("xRetentionDate", xRetentionDateData);
                binders.putLocal("dDocAccount", dDocAccountData);
                binders.putLocal("dDocAccount", dDocAccountData);
                binders.putLocal("dSecurityGroup", dSecurityGroupData);
                binders.putLocal("xRemarks", xRemarksData);
            try {
		Log.info("PHE:executeUpdateDocInfoMultiple:binder:" + binders);
                returnFunction = dColl.executeService(binders, userLogin, true);
                Log.info("PHE:executeUpdateDocInfoMultiple:returnFunction:" + returnFunction);
            } catch (Exception ex) {
                Log.error("PHE:executeUpdateDocInfoMultiple:ex:" + ex.getMessage().toString());
                return "executeFailed:updateStatus";
            }
        }

        return returnFunction;
    }
}
