package phecustomreservation;

import data.DatabaseValue;
import intradoc.common.Log;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.DataResultSet;
import intradoc.data.ResultSet;
import intradoc.data.ResultSetUtils;
import intradoc.data.Workspace;
import intradoc.provider.Provider;
import intradoc.provider.Providers;
import intradoc.server.Service;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import util.DataCollection;

public class PheCustomReservation extends Service
{
  public Workspace getSystemWorkspace()
  {
    Workspace workspace = null;
    Provider wsProvider = Providers.getProvider("SystemDatabase");
    if (wsProvider != null) {
      workspace = (Workspace)wsProvider.getProvider();
    }
    return workspace;
  }

  public void updatePheRequestItemStatus()
    throws DataException, ServiceException
  {
    Workspace ws = getSystemWorkspace();
    String dID = this.m_binder.getLocal("dID");
    String dPheRequestItemStatus = this.m_binder.getLocal("dPheRequestItemStatus");
    DataBinder contentBinder = new DataBinder();
    contentBinder.putLocal("dPheRequestItemStatus", dPheRequestItemStatus);
    contentBinder.putLocal("dID", dID);
    Log.info("PHE:dID : " + dID);
    Log.info("PHE:dPheRequestItemStatus : " + dPheRequestItemStatus);
    ws.execute("PheUpdateTicketItemStatus", contentBinder);
  }

  public void updatePheRequestItemStatusNew()
    throws DataException, ServiceException
  {
    Workspace ws = getSystemWorkspace();
    String dID = this.m_binder.getLocal("dID");
    String dRequestID = this.m_binder.getLocal("dRequestID");
    String dPheRequestItemStatus = this.m_binder.getLocal("dPheRequestItemStatus");
    DataBinder contentBinder = new DataBinder();
    contentBinder.putLocal("dPheRequestItemStatus", dPheRequestItemStatus);
    contentBinder.putLocal("dRequestID", dRequestID);
    contentBinder.putLocal("dID", dID);
    Log.info("PHE:dID : " + dID);
    Log.info("PHE:dRequestID : " + dRequestID);
    Log.info("PHE:dPheRequestItemStatus : " + dPheRequestItemStatus);
    ws.execute("PheUpdateTicketItemStatusNew", contentBinder);
  }

  public void queryInboxTicket()
    throws DataException, ServiceException
  {
    Workspace ws = getSystemWorkspace();
    String ItemStatus1 = this.m_binder.getLocal("ItemStatus1");
    String ItemStatus2 = this.m_binder.getLocal("ItemStatus2");
    String User = this.m_binder.getLocal("User");
    String Role = this.m_binder.getLocal("Role");
    DataBinder contentBinder = new DataBinder();

    DatabaseValue dValue = new DatabaseValue();
    Log.info("PHE:queryInboxTicket:role:" + Role);

    Log.info("PHE:queryInboxTicket:if:role:" + Role);

    ResultSet rSet = dValue.getTicketByAuthor(ItemStatus1, ItemStatus2, User, Role);
    DataResultSet myDataResultSet = new DataResultSet();
    myDataResultSet.copy(rSet);
    this.m_binder.addResultSet("pheticketRS", myDataResultSet);
  }

  public void checkPheTicketComplete()
    throws DataException, ServiceException
  {
    Workspace ws = getSystemWorkspace();
    String dRequestID = this.m_binder.getLocal("dRequestID");
    DataBinder contentBinder = new DataBinder();
    contentBinder.putLocal("dRequestID", dRequestID);
    ResultSet rset = ws.createResultSet("PheQueryDIsComplete", contentBinder);
    Log.info("PHE:Hasil Query Status Ticket" + rset);

    String dIsComplete = ResultSetUtils.getValue(rset, "dIsComplete");
    Log.info("PHE:Ticket Status: dIsComplete: " + dIsComplete);
    if (dIsComplete.equals("1")) {
      DataBinder binder2 = new DataBinder();
      binder2.putLocal("dPheStatus", "Complete");
      binder2.putLocal("dRequestID", dRequestID);

      ws.execute("PheUpdateTicketPheStatus", binder2);
    }
    else if (dIsComplete.equals("0"))
    {
      DataBinder binder2 = new DataBinder();
      binder2.putLocal("dPheStatus", "Return Partially");
      binder2.putLocal("dRequestID", dRequestID);
      ResultSet rs = ws.createResultSet("PheQueryReturn", binder2);

      if (rs.isRowPresent())
        ws.execute("PheUpdateTicketPheStatus", binder2);
    }
  }

  public void updateUserAcceptTask()
    throws DataException, ServiceException
  {
    Workspace ws = getSystemWorkspace();
    String pheAdminTable = this.m_binder.getLocal("pheAdminTable");
    String pheUser = this.m_binder.getLocal("pheUser");
    String dRequestID = this.m_binder.getLocal("dRequestID");
    String pheRequestItemStat = this.m_binder.getLocal("pheRequestItemStat");
    DataBinder contentBinder = new DataBinder();

    String query = "Update reservations set " + pheAdminTable + "='" + pheUser + "' where dRequestID='" + dRequestID + "'";
    String updateItemStatusTmp = "Update reservationitemmap set dpherequestitemstatustmp='" + pheRequestItemStat + "' where dRequestID='" + dRequestID + "'";

    Log.info("PHE:UpdateQuery : " + query);
    Log.info("PHE:UpdateItemStatusQuery : " + updateItemStatusTmp);
    String returnExecute1 = "";
    String returnExecute2 = "";
    try {
      returnExecute1 = String.valueOf(ws.executeSQL(query));
      returnExecute2 = String.valueOf(ws.executeSQL(updateItemStatusTmp));
    } catch (Exception ex) {
      Log.info("PHE:updateAcceptTask:" + ex.getLocalizedMessage());
      System.out.println(ex.getLocalizedMessage());
      returnExecute1 = "FAILED";
    } finally {
      try {
        ws.commitTran();
        ws.releaseConnection();
      } catch (DataException ex) {
        Log.error("PHE:updateAcceptTask:Commit:ex:" + ex.getLocalizedMessage());
        System.out.println(ex.getLocalizedMessage());
      }
    }
  }

  public void insertLogAcceptTask()
    throws DataException, ServiceException
  {
    DataResultSet result = null;
    String dRequestID = this.m_binder.getLocal("dRequestID");
    String pheAction = this.m_binder.getLocal("pheAction");
    String pheActionBy = this.m_binder.getLocal("pheActionBy");
    DateFormat dFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
    Date date = new Date();

    String query = "select dID from reservationitemmap where to_char(dRequestID)='" + dRequestID + "'";

    Log.info("---> (1)query : " + query);

    DataCollection dColl = new DataCollection();
    Workspace ws = dColl.getSystemWorkspace();
    ResultSet rs = ws.createResultSetSQL(query);
    result = new DataResultSet();
    result.copy(rs);

    Log.info("---> (2)Resultset : " + result);

    String insertQuery = "Insert all into phe_report_dls_actionlog (dRequestID,dID, pheActionDate, pheAction, pheActionBy) values ('";

    int numCols = result.getNumRows();

    Log.info("---> (3)numCols : " + numCols);

    for (int i = 0; i < numCols; i++) {
      Log.info("---> (4)hasNext result");

      insertQuery = insertQuery + dRequestID + "', '" + result.getStringValue(0) + "', " + "TO_DATE('" + dFormat.format(date) + "','YYYY-MM-DD HH24:MI:SS'), '" + pheAction + "', '" + pheActionBy + "') ";

      Log.info("---> (5)Message : " + insertQuery);
    }
    insertQuery = insertQuery + "select * from dual";
    System.out.println("---> (6)Message : " + insertQuery);
    Log.info("---> (7)InsertQuery : " + insertQuery);

    String returnExecute = "";
    try
    {
      returnExecute = String.valueOf(ws.executeSQL(insertQuery));
    } catch (Exception ex) {
      Log.info("PHE:insertLogAcceptTask:" + ex.getLocalizedMessage());
      System.out.println(ex.getLocalizedMessage());
      returnExecute = "FAILED";
    } finally {
      try {
        ws.commitTran();
        ws.releaseConnection();
      } catch (DataException ex) {
        Log.error("PHE:insertLogAcceptTask:Commit:ex:" + ex.getLocalizedMessage());
        System.out.println(ex.getLocalizedMessage());
      }
    }
  }

  public void insertActionLog1()
    throws DataException, ServiceException
  {
    DataResultSet result = null;
    String dRequestID = this.m_binder.getLocal("dRequestID");
    String dID = this.m_binder.getLocal("dID");
    String pheAction = this.m_binder.getLocal("pheAction");
    String pheActionBy = this.m_binder.getLocal("pheActionBy");
    DateFormat dFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
    Date date = new Date();

    String insertQuery = "Insert into phe_report_dls_actionlog (dRequestID,dID, pheActionDate, pheAction, pheActionBy) values ('";

    insertQuery = insertQuery + dRequestID + "', '" + dID + "', " + "TO_DATE('" + dFormat.format(date) + "','YYYY-MM-DD HH24:MI:SS'), '" + pheAction + "', '" + pheActionBy + "') ";

    System.out.println("---> (1)Insert Query : " + insertQuery);
    DataCollection dColl = new DataCollection();
    Workspace ws = dColl.getSystemWorkspace();

    Log.info("---> (1)InsertQuery : " + insertQuery);
    String returnExecute = "";
    try
    {
      returnExecute = String.valueOf(ws.executeSQL(insertQuery));
    } catch (Exception ex) {
      Log.info("PHE:insertLog1:" + ex.getLocalizedMessage());
      System.out.println(ex.getLocalizedMessage());
      returnExecute = "FAILED";
    } finally {
      try {
        ws.commitTran();
        ws.releaseConnection();
      } catch (DataException ex) {
        Log.error("PHE:insertLog1:Commit:ex:" + ex.getLocalizedMessage());
        System.out.println(ex.getLocalizedMessage());
      }
    }
  }

  public void insertActionLogExtend()
    throws DataException, ServiceException
  {
    DataResultSet result = null;
    String dRequestID = this.m_binder.getLocal("dRequestID");
    String dID = this.m_binder.getLocal("dID");
    String pheAction = this.m_binder.getLocal("pheAction");
    String pheActionBy = this.m_binder.getLocal("pheActionBy");
    String pheOldReturnDate = this.m_binder.getLocal("pheOldReturnDate");
    String pheNewReturnDate = this.m_binder.getLocal("pheNewReturnDate");
    DateFormat dFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
    Date date = new Date();

    String insertQuery = "Insert into phe_report_dls_actionlog (dRequestID,dID, pheActionDate, pheAction, pheActionBy, pheOldReturnDate, pheNewReturnDate) values ('";

    insertQuery = insertQuery + dRequestID + "', '" + dID + "', " + "TO_DATE('" + dFormat.format(date) + "','YYYY-MM-DD HH24:MI:SS'), '" + pheAction + "', '" + pheActionBy + "', " + "TO_DATE('" + pheOldReturnDate + "','MM/DD/YYYY'), " + "TO_DATE('" + pheNewReturnDate + "','MM/DD/YYYY') )";

    System.out.println("---> (1)Insert Query : " + insertQuery);
    Log.info("---> (1)InsertQuery : " + insertQuery);
    DataCollection dColl = new DataCollection();
    Workspace ws = dColl.getSystemWorkspace();

    String returnExecute = "";
    try
    {
      returnExecute = String.valueOf(ws.executeSQL(insertQuery));
    } catch (Exception ex) {
      Log.info("PHE:insertLog1:" + ex.getLocalizedMessage());
      System.out.println(ex.getLocalizedMessage());
      returnExecute = "FAILED";
    } finally {
      try {
        ws.commitTran();
        ws.releaseConnection();
      } catch (DataException ex) {
        Log.error("PHE:insertLog1:Commit:ex:" + ex.getLocalizedMessage());
        System.out.println(ex.getLocalizedMessage());
      }
    }
  }

  public void pheTicketProcessData()
    throws DataException, ServiceException
  {
    Workspace ws = getSystemWorkspace();
    String ItemStatus1 = this.m_binder.getLocal("ItemStatus1");
    String ItemStatus2 = this.m_binder.getLocal("ItemStatus2");
    String dUser = this.m_binder.getLocal("dUser");
    //modified by nanda - 1201 - untuk menghilangkan flow tiket yang nempel ke username, tapi cukup nempel ke role
    //modified by Gumas - 20160413 - adminTable diaktifkan kembali, digunakan untuk membedakan admin warehouse dgn admin tdc atau divisi. flow tiket tetap menmpel pada role
    String adminTable = this.m_binder.getLocal("adminTable");
    
    DataBinder contentBinder = new DataBinder();

    DatabaseValue dValue = new DatabaseValue();

    ResultSet rSet = dValue.pheTicketProcessListData(ItemStatus1, ItemStatus2, dUser, adminTable);
    DataResultSet myDataResultSet = new DataResultSet();
    myDataResultSet.copy(rSet);
    Log.info("PHE:Hasil Query" + myDataResultSet);
    System.out.println(myDataResultSet);
    this.m_binder.addResultSet("pheTicketRS", myDataResultSet);
  }

  public void updatePheMultipleExtend()
    throws DataException, ServiceException
  {
    DateFormat dFormat = new SimpleDateFormat("mm/dd/yyyy");
    Date date = new Date();

    String dPheRequestExtendDate = this.m_binder.getLocal("dPheRequestExtendDate");
    String dPheRequestItemStatus = this.m_binder.getLocal("dPheRequestItemStatus");
    String dPheRequestItemStatusTmp = this.m_binder.getLocal("dPheRequestItemStatusTmp");
    String dRequestID = this.m_binder.getLocal("dRequestID");
    String dID = this.m_binder.getLocal("dID");

    String insertQuery = "UPDATE RESERVATIONITEMMAP SET DPHEREQUESTEXTENDDATE=TO_DATE('" + dPheRequestExtendDate + "', 'mm/dd/yyyy')";
    insertQuery = insertQuery + ", DPHEREQUESTITEMSTATUS='" + dPheRequestItemStatus;
    insertQuery = insertQuery + "', DPHEREQUESTITEMSTATUSTMP='" + dPheRequestItemStatusTmp;
    insertQuery = insertQuery + "' WHERE DREQUESTID = " + dRequestID;
    insertQuery = insertQuery + " AND DID IN (" + dID + ")";

    System.out.println("---> (1)Insert Query : " + insertQuery);
    DataCollection dColl = new DataCollection();
    Workspace ws = dColl.getSystemWorkspace();

    Log.info("---> (1)InsertQuery : " + insertQuery);
    String returnExecute = "";
    try
    {
      returnExecute = String.valueOf(ws.executeSQL(insertQuery));
    } catch (Exception ex) {
      Log.info("PHE:insertLog1:" + ex.getLocalizedMessage());
      System.out.println(ex.getLocalizedMessage());
      returnExecute = "FAILED";
    } finally {
      try {
        ws.commitTran();
        ws.releaseConnection();
      } catch (DataException ex) {
        Log.error("PHE:insertLog1:Commit:ex:" + ex.getLocalizedMessage());
        System.out.println(ex.getLocalizedMessage());
      }
    }
  }
}