package checkin;

import data.DatabaseValue;
import intradoc.common.ExecutionContext;
import intradoc.common.Log;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.ResultSet;
import intradoc.data.Workspace;
import intradoc.shared.FilterImplementor;
import intradocservice.Folder;
import intradocservice.Util;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.Validator;

public class CheckinFilter implements FilterImplementor {
	private String location;
	private String platform;
	private String disCode;
	private String docType;
	private String parentFolder;
	private String runNumber;
	private String secGroupApDrawing = "PHEDrawing";
	private String secGroupApNonDrawing = "PHENonDrawing";
	private String secGroupApTdc = "PHE_TDC";
	private String docTypeApDrawing = "Drawing";
	private String docTypeApNonDrawing = "NonDrawing";
	private String docTypeApTdcDrawing = "MasterListDrawing";
	private String docTypeApTdcNonDrawing = "MasterListNonDrawing";
	private String cabinetApMain = "";
	private String folderTechDocONWJ = "Technical Documents";
	private String folderTechOtherAP = "STK Documents";
	private String folderNative = "Native Documents";

	public void setRunNumber(String runNumber) {
		this.runNumber = runNumber;
	}

	public String getRunNumber() {
		return this.runNumber;
	}

	public void setParentFolder(String parentFolder) {
		this.parentFolder = parentFolder;
	}

	public String getParentFolder() {
		return this.parentFolder;
	}

	public void SetStatus(String status, DataBinder binder) {
		binder.putLocal("xStatus", status);
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLocation() {
		return this.location;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getPlatform() {
		return this.platform;
	}

	public void setDisCode(String disCode) {
		this.disCode = disCode;
	}

	public String getDisCode() {
		return this.disCode;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getDocType() {
		return this.docType;
	}

	public int doFilter(Workspace ws, DataBinder binder, ExecutionContext cxt) throws DataException, ServiceException {
		DatabaseValue dValue = new DatabaseValue();
		Validator valid = new Validator();

		String pheAp = binder.getLocal("xInternalOwner") == null ? "" : binder.getLocal("xInternalOwner");
		if (pheAp.length() <= 0) {
			String apComp = binder.getLocal("xCompany") == null ? "" : binder.getLocal("xCompany");
			pheAp = valid.verifyApPrefix(apComp);
		} else {
			pheAp = pheAp.replace(" ", "").toUpperCase();
		}
		if (pheAp.length() > 0) {
			String keyConfApSGD = "SECGRP_DRAWING_" + pheAp;
			try {
				secGroupApDrawing = dValue.getPheConfig(keyConfApSGD.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(CheckinFilter.class.getName()).log(Level.SEVERE, null, ex);
			}
			String keyConfApSGND = "SECGRP_NONDRAWING_" + pheAp;
			try {
				secGroupApNonDrawing = dValue.getPheConfig(keyConfApSGND.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(CheckinFilter.class.getName()).log(Level.SEVERE, null, ex);
			}
			String keyConfApDTD = "DOCTYP_DRAWING_" + pheAp;
			try {
				docTypeApDrawing = dValue.getPheConfig(keyConfApDTD.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(CheckinFilter.class.getName()).log(Level.SEVERE, null, ex);
			}
			String keyConfApDTND = "DOCTYP_NONDRAWING_" + pheAp;
			try {
				docTypeApNonDrawing = dValue.getPheConfig(keyConfApDTND.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(CheckinFilter.class.getName()).log(Level.SEVERE, null, ex);
			}
			String keyConfApTDC = "SECGRP_TDC_" + pheAp;
			try {
				secGroupApTdc = dValue.getPheConfig(keyConfApTDC.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(CheckinFilter.class.getName()).log(Level.SEVERE, null, ex);
			}
			String keyConfApCab = "CABINETS_" + pheAp;
			try {
				cabinetApMain = dValue.getPheConfig(keyConfApCab.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(CheckinFilter.class.getName()).log(Level.SEVERE, null, ex);
			}
			String keyConfApDTTD = "DOCTYP_TDC_DRAWING_" + pheAp;
			try {
				docTypeApTdcDrawing = dValue.getPheConfig(keyConfApDTTD.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(CheckinFilter.class.getName()).log(Level.SEVERE, null, ex);
			}
			String keyConfApDTTND = "DOCTYP_TDC_NONDRAWING_" + pheAp;
			try {
				docTypeApTdcNonDrawing = dValue.getPheConfig(keyConfApDTTND.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(CheckinFilter.class.getName()).log(Level.SEVERE, null, ex);
			}
		} else {
			Log.info("PHE:CheckinFilter.java:doFilter: AP Parameter not defined !");
		}
		String updateStatus = binder.getLocal("isUpdate");
		boolean isUpdate;
		if (updateStatus == null) {
			isUpdate = false;
		} else {
			if (updateStatus.equalsIgnoreCase("0")) {
				isUpdate = false;
			} else {
				isUpdate = true;
			}
		}
		Folder fld = new Folder();
		Util util = new Util();
		util.setUserLogin(binder.getLocal("dUser"));
		String type = binder.getLocal("dDocType");

		boolean isCustom = false;
		try {
			isCustom = Boolean.valueOf(binder.getLocal("isCustom").toString()).booleanValue();
		} catch (Exception ex) {
			if (type.toUpperCase().contains("MASTERLIST")) {
				isCustom = true;
			} else {
				Log.error("PHE:CheckinFilter.java:doFilter:ex: " + ex.getLocalizedMessage());
			}
		}
		if (isCustom) {
			if (!isUpdate) {
				if (type.toUpperCase().contains("MASTERLIST")) {
					String parentTechFolder = (pheAp.toUpperCase().equalsIgnoreCase("PHEONWJ") ? folderTechDocONWJ
							: folderTechOtherAP);
					String parentDir = cabinetApMain + "/" + parentTechFolder;
					String docNumber = "";
					String area = "";
					String platform = "";
					String disciplineCode = "";
					String docType = "";
					String runningNumber = "";
					String sheetNumber = "";
					String folderPath = "";
					Boolean isTDCDrawing = Boolean.valueOf(true);
					if (type.equalsIgnoreCase(docTypeApTdcDrawing)) {
						docNumber = binder.getLocal("xDocNumber");
						area = binder.getLocal("xDLocation");
						platform = binder.getLocal("xDPlatform");
						disciplineCode = binder.getLocal("xDDisciplineCode");
						docType = binder.getLocal("xDDocTypeCode");
						sheetNumber = binder.getLocal("xSheetNumber");
						runningNumber = binder.getLocal("xRunningNumber");
						docNumber = area + "-" + platform + "-" + disciplineCode + "-" + docType + "-" + runningNumber;
						folderPath = area + "/" + platform + "/" + fld.GetDisCodeDesc(type, disciplineCode, ws) + "/"
								+ docType;
						parentDir = parentDir + "/Drawings";
						isTDCDrawing = Boolean.valueOf(true);
						binder.putLocal("dSecurityGroup", secGroupApDrawing);
						binder.putLocal("fSecurityGroup", secGroupApDrawing);
					} else if (type.equalsIgnoreCase(docTypeApTdcNonDrawing)) {
						docNumber = binder.getLocal("xDocNumber");
						area = binder.getLocal("xNdLocation");
						disciplineCode = binder.getLocal("xNdDisciplineCode");
						docType = binder.getLocal("xNdDocTypeCode");
						runningNumber = binder.getLocal("xRunningNumber");
						docNumber = area + "-" + disciplineCode + "-" + docType + "-" + runningNumber;
						folderPath = area + "/" + fld.GetDisCodeDesc(type, disciplineCode, ws) + "/" + docType;
						parentDir = parentDir + "/Non Drawings";
						isTDCDrawing = Boolean.valueOf(false);
						binder.putLocal("dSecurityGroup", secGroupApNonDrawing);
						binder.putLocal("fSecurityGroup", secGroupApNonDrawing);
					} else {
						Log.error("docTypeApTdc config untuk tipe " + type + " di PHE_CONFIG salah.");
					}
					binder.putLocal("xDocNumber", docNumber);
					binder.putLocal("xStatus", "Registered");
					runningNumber = binder.getLocal("xRunningNumber");
					runningNumber = String.format("%04d",
							new Object[] { Integer.valueOf(Integer.parseInt(runningNumber.trim())) });
					if (!fld.IsContentExist(ws, area, platform, disciplineCode, docType, runningNumber, sheetNumber,
							isTDCDrawing.booleanValue())) {
						if (!fld.DoCheckingFolder(parentDir, folderPath, binder, ws, util)) {
							Log.error(
									"PHE:CheckinFilter.java:doFilter:Failed to checkin folder. Users might be haven't priviledges to access or create the folder directory.");
							return -1;
						}
						ResultSet rs = fld.GetFolderInfo(parentDir + "/" + folderPath, binder, ws, util);
						if (!rs.isEmpty()) {
							binder.putLocal("fParentGUID", rs.getStringValueByName("fFolderGUID"));
						}
						binder.putLocal("AutoNumberPrefix", "EDMS");

						return 0;
					}
					binder.putLocal("ermStatusMessage", "Content Already Exists");
					return -1;
				}
				if ((type.equalsIgnoreCase(docTypeApDrawing)) || (type.equalsIgnoreCase(docTypeApNonDrawing))
						|| (type.equalsIgnoreCase("NonDrawingPassive"))) {
					String docPurpose = binder.getLocal("xDocPurpose");
					if (type.toUpperCase().contains("NONDRAWING")) {
						this.location = binder.getLocal("xNdLocation");
						this.disCode = binder.getLocal("xNdDisciplineCode");
						this.docType = binder.getLocal("xNdDocTypeCode");
						if (docPurpose.toUpperCase().contains("NATIVE")) {
							binder.putLocal("dSecurityGroup", this.secGroupApTdc);
							binder.putLocal("fSecurityGroup", this.secGroupApTdc);
						} else {
							binder.putLocal("dSecurityGroup", this.secGroupApNonDrawing);
							binder.putLocal("fSecurityGroup", this.secGroupApNonDrawing);
						}
					} else {
						this.location = binder.getLocal("xDLocation");
						this.platform = binder.getLocal("xDPlatform");
						this.disCode = binder.getLocal("xDDisciplineCode");
						this.docType = binder.getLocal("xDDocTypeCode");
						if (docPurpose.toUpperCase().contains("NATIVE")) {
							binder.putLocal("dSecurityGroup", secGroupApTdc);
							binder.putLocal("fSecurityGroup", secGroupApTdc);
						} else {
							binder.putLocal("dSecurityGroup", secGroupApDrawing);
							binder.putLocal("fSecurityGroup", secGroupApDrawing);
						}
					}
					binder.putLocal("createPrimaryMetaFile", "0");
					this.parentFolder = (docPurpose.toUpperCase().equalsIgnoreCase("NATIVE") ? folderNative
							: type.toUpperCase().equals("NONDRAWING") || type.toUpperCase().equals("NONDRAWINGPASSIVE")
									? folderTechDocONWJ
									: type.toUpperCase().equals("DRAWING") ? folderTechDocONWJ : folderTechOtherAP);

					PrepareCheckin(binder, type, ws, util, cabinetApMain);
					if ((binder.getLocal("GetFileBack") != null) && (binder.getLocal("GetFileBack").equals("true"))) {
						binder.putLocal("IsJava", "1");
						binder.addTempFile(binder.getLocal("primaryFile:path"));
						File f = new File(binder.getLocal("primaryFile"));
						binder.putLocal("primaryFile", f.getName());
					}
					try {
						String documentNameDipotong = binder.getLocal("xDocName");
						if (documentNameDipotong.indexOf(" ") >= 0) {
							documentNameDipotong = documentNameDipotong.substring(0, documentNameDipotong.indexOf(" "));
							binder.putLocal("xDocName", documentNameDipotong);
						} else if (documentNameDipotong.indexOf(".") >= 0) {
							documentNameDipotong = documentNameDipotong.substring(0, documentNameDipotong.indexOf("."));
							binder.putLocal("xDocName", documentNameDipotong);
						}
					} catch (Exception ex) {
					}
					return 0;
				}
				if (type.equalsIgnoreCase("Document")) {
					String primaryFile = binder.getLocal("primaryFile");
					String parentDir = "";
					String folderPath = "BatchLog";
					if (binder.getLocal("dDocTitle").toString().indexOf("Upload Failed") >= 0) {
						fld.DoCheckingFolder(parentDir, folderPath, binder, ws, util);

						ResultSet rs = fld.GetFolderInfo(parentDir + "/" + folderPath, binder, ws, util);
						if (!rs.isEmpty()) {
							binder.putLocal("fParentGUID", rs.getStringValueByName("fFolderGUID"));
						}
						binder.putLocal("primaryFile:path", primaryFile);

						return 0;
					}
				} else if (type.equalsIgnoreCase("General")) {
					boolean isJavaMethod = Boolean.parseBoolean(binder.getLocal("isJavaMethod"));
					if (isJavaMethod) {
						String primaryFile = binder.getLocal("primaryFile");
						binder.putLocal("primaryFile:path", primaryFile);
						fld.DoCheckingFolder("", binder.getLocal("destinationPath"), binder, ws, util);

						ResultSet rs = fld.GetFolderInfo(binder.getLocal("destinationPath"), binder, ws, util);
						if (!rs.isEmpty()) {
							binder.putLocal("fParentGUID", rs.getStringValueByName("fFolderGUID"));
						}
						if ((binder.getLocal("GetFileBack") != null)
								&& (binder.getLocal("GetFileBack").equals("true"))) {
							binder.putLocal("IsJava", "1");
							binder.addTempFile(binder.getLocal("primaryFile:path"));
							File f = new File(binder.getLocal("primaryFile"));
							binder.putLocal("primaryFile", f.getName());
						}
						try {
							String documentNameDipotong = binder.getLocal("xDocName");
							if (documentNameDipotong.indexOf(".") >= 0) {
								documentNameDipotong = documentNameDipotong.substring(0,
										documentNameDipotong.indexOf("."));
								binder.putLocal("xDocName", documentNameDipotong);
							}
						} catch (Exception ex) {
						}
					}
					return 0;
				}
			} else if (type.toUpperCase().contains("MASTERLIST")) {
				if (!type.equalsIgnoreCase(this.docTypeApTdcDrawing)) {
					type.equalsIgnoreCase(this.docTypeApTdcNonDrawing);
				}
			} else if ((type.equalsIgnoreCase(this.docTypeApDrawing))
					|| (type.equalsIgnoreCase(this.docTypeApNonDrawing)) || (type.equalsIgnoreCase("NonDrawingPassive"))
					|| (type.equalsIgnoreCase("General"))) {
				if ((type.toUpperCase().contains("NONDRAWING")) && (binder.getLocal("GetFileBack") != null)
						&& (binder.getLocal("GetFileBack").equals("true"))) {
					binder.putLocal("primaryFile:path", binder.getLocal("primaryFile"));
					binder.putLocal("IsJava", "1");
					binder.addTempFile(binder.getLocal("primaryFile"));
					File f = new File(binder.getLocal("primaryFile"));
					binder.putLocal("primaryFile", f.getName());
				}
				binder.putLocal("createPrimaryMetaFile", "0");
			}
		}
		return 0;
	}

	public boolean PrepareCheckin(DataBinder binder, String type, Workspace ws, Util util, String cabinetApMain)
			throws ServiceException {
		boolean result;
		try {
			Folder fld = new Folder();
			String parentDir = "/" + cabinetApMain + "/" + this.parentFolder;
			String folderPath;
			if (type.toUpperCase().contains("NONDRAWING") || type.toUpperCase().contains("NONDRAWINGPASSIVE")) {
				parentDir = parentDir + "/Non Drawings";
				folderPath = this.location + "/" + fld.GetDisCodeDesc(type, this.disCode, ws) + "/" + this.docType;
			} else {
				parentDir = parentDir + "/Drawings";
				folderPath = this.location + "/" + this.platform + "/" + fld.GetDisCodeDesc(type, this.disCode, ws)
						+ "/" + this.docType;
			}
			if (fld.DoCheckingFolder(parentDir, folderPath, binder, ws, util)) {
				ResultSet rs = fld.GetFolderInfo(parentDir + "/" + folderPath, binder, ws, util);
				if (rs != null) {
					binder.putLocal("fParentGUID", rs.getStringValueByName("fFolderGUID"));
				}
				result = true;
			} else {
				result = false;
			}
		} catch (Exception ex) {
			Log.error("PHE:CheckinFilter.java:PrepareCheckin:ex: " + ex.getLocalizedMessage());
			result = false;
		}
		return result;
	}

	public String GetFolderGUID(DataBinder binder, String type, Workspace ws, Util util) throws ServiceException {
		try {
			Folder fld = new Folder();

			String parentDir = "/Cabinets/" + this.parentFolder;
			String folderPath;
			if (type.toUpperCase().contains("NONDRAWING")) {
				parentDir = parentDir + "/Non Drawings";
				folderPath = this.location + "/" + fld.GetDisCodeDesc(type, this.disCode, ws) + "/" + this.docType;
			} else {
				parentDir = parentDir + "/Drawings";
				folderPath = this.location + "/" + this.platform + "/" + fld.GetDisCodeDesc(type, this.disCode, ws)
						+ "/" + this.docType;
			}
			if (fld.DoCheckingFolder(parentDir, folderPath, binder, ws, util)) {
				ResultSet rs = fld.GetFolderInfo(parentDir + "/" + folderPath, binder, ws, util);
				if (rs != null) {
					return rs.getStringValueByName("fFolderGUID");
				}
			}
			return "";
		} catch (Exception ex) {
			Log.error("PHE:CheckinFilter.java:GetFolderGUID:ex: " + ex.getLocalizedMessage());
		}
		return "";
	}

	public String getFolderPath(DataBinder binder, String type, Workspace ws, Util util, String cabinetApMain)
			throws ServiceException {
		Folder fld = new Folder();

		String parentDir = cabinetApMain + "/" + this.parentFolder;
		String folderPath;
		if (type.toUpperCase().contains("NONDRAWING") || type.toUpperCase().equals("NONDRAWINGPASSIVE")) {
			parentDir = parentDir + "/Non Drawings";
			folderPath = this.location + "/" + fld.GetDisCodeDesc(type, this.disCode, ws) + "/" + this.docType;
		} else {
			parentDir = parentDir + "/Drawings";
			folderPath = this.location + "/" + this.platform + "/" + fld.GetDisCodeDesc(type, this.disCode, ws) + "/"
					+ this.docType;
		}
		return parentDir + "/" + folderPath;
	}

	public boolean IsValidDocNumber(String docNumber, String type) {
		String[] names = docNumber.split("-");
		if (type.toUpperCase().contains("NONDRAWING")) {
			this.location = names[0];
			this.disCode = names[1];
			this.docType = names[2];
			this.runNumber = names[3];
			if (!isCombinationNotNull(type)) {
				return false;
			}
			DatabaseValue db = new DatabaseValue();
			try {
				this.location = db.getLocation(this.location, type);
				this.disCode = db.getDisciplineCode(this.disCode, type);
				this.docType = db.getDocumentType(this.docType, type);
				if (!isCombinationNotNull(type)) {
					return false;
				}
			} catch (DataException e) {
				return false;
			}
		} else {
			this.location = names[0];
			this.platform = names[1];
			this.disCode = names[2];
			this.docType = names[3];
			this.runNumber = names[4];
			if (!isCombinationNotNull(type)) {
				return false;
			}
			DatabaseValue db = new DatabaseValue();
			try {
				this.location = db.getLocation(this.location, type);
				this.platform = db.getPlatform(this.location, this.platform);
				this.disCode = db.getDisciplineCode(this.disCode, type);
				this.docType = db.getDocumentType(this.docType, type);
				if (!isCombinationNotNull(type)) {
					return false;
				}
			} catch (DataException e) {
				return false;
			}
		}
		return true;
	}

	public boolean isCombinationNotNull(String type) {
		if (type.toUpperCase().contains("NONDRAWING")) {
			if ((this.location.isEmpty()) || (this.disCode.isEmpty()) || (this.docType.isEmpty())) {
				return false;
			}
		} else if ((this.location.isEmpty()) || (this.platform.isEmpty()) || (this.disCode.isEmpty())
				|| (this.docType.isEmpty())) {
			return false;
		}
		return true;
	}
}
