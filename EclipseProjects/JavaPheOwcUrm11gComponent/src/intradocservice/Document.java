package intradocservice;

import data.DatabaseValue;

import intradoc.common.Log;
import intradoc.common.ServiceException;

import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.ResultSet;
import intradoc.data.Workspace;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import util.DataCollection;
import util.Validator;

public class Document {
	private String uploadTempHeaderMasterlistDrawing = "TDC MASTER LIST DRAWING";
	private String uploadTempHeaderMasterlistNonDrawing = "TDC MASTER LIST NONDRAWING";
	private String uploadTempHeaderGeneral = "DCRMS GENERAL - GENERAL DOCUMENT";
	private String uploadTempHeaderPublic = "DCRMS PHE PUBLIC - PUBLIC DOCUMENT";
	private String uploadTempHeaderDrawing = "DCRMS GENERAL - DRAWING DOCUMENT";
	private String uploadTempHeaderNonDrawing = "DCRMS GENERAL - NONDRAWING DOCUMENT";
	//private String uploadTempHeaderExternalResource = "EXTERNAL RESOURCE LIST";

    public Document() {
        super();
    }

    public ResultSet GetDocInfo(String dID, DataBinder binder, Workspace ws, boolean isJavaMethod, Util util) {
        DataCollection dColl = new DataCollection();
        try {
            binder.putLocal("dID", dID);
            String serviceName = "DOC_INFO";
            binder.putLocal("IdcService", serviceName);
			if (isJavaMethod) {
                dColl.executeService(binder, binder.getLocal("dUser"), true);
            } else {
                util.executeService(binder, util.getUserLogin(), false, ws);
            }
            ResultSet rs = binder.getResultSet("DOC_INFO");
            return rs;
        } catch (DataException e) {
            Log.error("PHE:GetDocInfo:" + e.getMessage());
            return null;
        } catch (ServiceException e) {
            Log.error(e.getMessage());
            return null;
        }
    }
    
    public ResultSet GetDocInfoFolderID(String dID, DataBinder binder, Workspace ws, boolean isJavaMethod, Util util) {
        DataCollection dColl = new DataCollection();
        try {
            binder.putLocal("dID", dID);
            String serviceName = "DOC_INFO";
            binder.putLocal("IdcService", serviceName);
            if (isJavaMethod) {
                dColl.executeService(binder, binder.getLocal("dUser"), true);
            } else {
                util.executeService(binder, util.getUserLogin(), false, ws);
            }
            ResultSet rs = binder.getResultSet("FileInfo");
            return rs;
        } catch (DataException e) {
            return null;
        } catch (ServiceException e) {
            Log.error(e.getMessage());
            return null;
        }
    }

    public ResultSet GetDocInfoLatestRelease(DataBinder binder, Workspace ws, boolean isJavaMethod, Util util) {
        DataCollection dColl = new DataCollection();
        try {
            String serviceName = "DOC_INFO_LATESTRELEASE";
            binder.putLocal("IdcService", serviceName);
            if (isJavaMethod) {
                dColl.executeService(binder, binder.getLocal("dUser"), true);
            } else {
                util.executeService(binder, util.getUserLogin(), false, ws);
            }
            ResultSet rs = binder.getResultSet("DOC_INFO");
            return rs;
        } catch (DataException e) {
            Log.error(e.getMessage());
            return null;
        } catch (ServiceException e) {
            Log.error(e.getMessage());
            return null;
        }
    }

    public ResultSet GetDocInfoByName(DataBinder binder, Workspace ws, boolean isJavaMethod, Util util) {
        DataCollection dColl = new DataCollection();
        try {
            String serviceName = "DOC_INFO_BY_NAME";
            binder.putLocal("IdcService", serviceName);
            if (isJavaMethod) {
                dColl.executeService(binder, binder.getLocal("dUser"), true);
            } else {
                util.executeService(binder, util.getUserLogin(), false, ws);
            }
            ResultSet rs = binder.getResultSet("DOC_INFO");
            return rs;
        } catch (DataException e) {
            Log.error(e.getMessage());
            return null;
        } catch (ServiceException e) {
            Log.error(e.getMessage());
            return null;
        }
    }

    public ResultSet getExpiredDocument() {
        DataCollection dColl = new DataCollection();
        DataBinder binder = new DataBinder();
        try {
            String serviceName = "GET_EXPIRED";
            binder.putLocal("IdcService", serviceName);
            dColl.executeService(binder, "weblogic", true);
            ResultSet rs = binder.getResultSet("EXPIRED_LIST");
            return rs;
        } catch (DataException e) {
            Log.error(e.getMessage());
            return null;
        } catch (ServiceException e) {
            Log.error(e.getMessage());
            return null;
        }
    }

    public String GetDocID(Workspace ws, String docNumber, String sheetNumber, boolean isNative) {
        DatabaseValue db = new DatabaseValue();
        String query;
        query = "SELECT R.dID FROM REVISIONS R "
                + "INNER JOIN DOCMETA DM ON R.DID = DM.DID "
                + "WHERE DM.XDOCNUMBER LIKE '" + docNumber + "' "
                + "AND DDOCTYPE LIKE '%Drawing%' ";
        if (sheetNumber.length() > 0) {
            query += "AND DM.XSHEETNUMBER LIKE '" + sheetNumber + "' ";
        } else {
            query += "AND (DM.XSHEETNUMBER IS NULL OR DM.XSHEETNUMBER = '0') ";
        }

        if (!isNative) {
            query = query + "AND UPPER(DM.XSTATUS) NOT LIKE 'NATIVE' ";
        } else {
            query = query + "AND UPPER(DM.XSTATUS) LIKE 'NATIVE' ";
        }

        query += "ORDER BY R.dID DESC";

        String ID = db.GetResultQuery(query, ws);
        
        return ID == null ? "0" : ID.length() <= 0 ? "0" : ID;
    }

    public String GetDocID(Workspace ws, String docNumber, String sheetNumber, String revision, boolean isNative) {
        DatabaseValue db = new DatabaseValue();
        String query;
        query = "SELECT R.dID FROM REVISIONS R "
                + "INNER JOIN DOCMETA DM ON R.DID = DM.DID "
                + "WHERE DM.XDOCNUMBER LIKE '" + docNumber + "' "
                + "AND DDOCTYPE LIKE '%Drawing%' ";
        if (sheetNumber.length() > 0) {
            query += "AND DM.XSHEETNUMBER LIKE '" + sheetNumber + "' ";
        } else {
            query += "AND (DM.XSHEETNUMBER IS NULL OR DM.XSHEETNUMBER = '0') ";
        }

        if (revision.length() > 0) {
            query += "AND DM.XREVISION LIKE '" + revision + "' ";
        } else {
            query += "AND DM.XREVISION IS NULL ";
        }

        if (isNative) {
            query = query + "AND UPPER(DM.XSTATUS) LIKE 'NATIVE' ";
        } else {
            query = query + "AND (DM.XDOCPURPOSE IS NOT NULL AND UPPER(DM.XSTATUS) NOT LIKE 'NATIVE') ";
        }

        query += "ORDER BY R.dID DESC";

        String ID = db.GetResultQuery(query, ws);
        
        return ID == null ? "0" : ID.length() <= 0 ? "0" : ID;
    }

    public String GetDocIDRegistered(Workspace ws, String docNumber, String sheetNumber) {
        DatabaseValue db = new DatabaseValue();
        String query;
        query = "SELECT R.dID FROM REVISIONS R "
                + "INNER JOIN DOCMETA DM ON R.DID = DM.DID "
                + "WHERE DM.XDOCNUMBER LIKE '" + docNumber + "' "
                + "AND DDOCTYPE LIKE '%Drawing%'  ";
        if (sheetNumber.length() > 0) {
            query += "AND DM.XSHEETNUMBER LIKE '" + sheetNumber + "'";
        } else {
            query += "AND (DM.XSHEETNUMBER IS NULL OR DM.XSHEETNUMBER = '0')";
        }
		query = query + "AND DM.XDOCPURPOSE IS NULL AND (UPPER(DM.XSTATUS) = 'REGISTERED' OR UPPER(DM.XSTATUS) = 'DELETED') ";

        query += " ORDER BY R.dID DESC";

        String ID = db.GetResultQuery(query, ws);
        
        return ID == null ? "0" : ID.length() <= 0 ? "0" : ID;
    }

    public boolean IsNative(Workspace ws, String dID) {
        DatabaseValue db = new DatabaseValue();
        String query;
        query = "SELECT DM.XDOCPURPOSE FROM REVISIONS R "
                + "INNER JOIN DOCMETA DM ON R.DID = DM.DID "
                + "WHERE R.DID LIKE '" + dID + "'";

        return db.GetResultQuery(query, ws).equalsIgnoreCase("NATIVE") ? true : false;
    }

    public boolean IsPublished(ResultSet rs) {
        return rs.getStringValueByName("xStatus").equalsIgnoreCase("PUBLISHED") ? true : false;
    }

    public boolean IsStatusPublished(ResultSet rs) {
        return rs.getStringValueByName("xStatus").equalsIgnoreCase("PUBLISHED") ? true : false;
    }

    public String GetDocInfoValue(String var, DataBinder binder) {
        return binder.getLocal("DOC_INFO." + var);
    }

    public String executeUpdateWithCheckin(
            DataBinder binder,
            String typeDrawing,
            String indexRow,
            String area,
            String platform,
            String disciplineCode,
            String documentType,
            String runningNumber,
            String sheetNumber,
            String documentTitle,
            String oldDocNo,
            String projectName,
            String projectYear,
            String ownerOrganization,
            String ownerName,
            String author,
            String remarks,
            String userLogin,
            String docTitle,
            String securityGroup,
            String docClasification,
            String sheetName,
            String error,
            String documentName,
            String documentNumber,
            String authorOrganization,
            String company,
            String retentionPeriod,
            String buAssetFunction,
            String destinationPath,
            String folderSource,
            String MOCNo,
            String authorGroupName,
            String contractor,
            String contractNo,
            String WONo,
            String revision,
            String status,
            String documentPurpose,
            String dDocName,
            String dID,
            String activePassive,
            String retentionDate,
            String docModel,
            String description,
            boolean isJavaMethod)
            throws DataException, ServiceException {
        String returnFunction = "";
        DataCollection dColl = new DataCollection();
        Workspace workspace = dColl.getSystemWorkspace();

        DataBinder binders = new DataBinder();
        binders.putLocal("isJavaMethod", String.valueOf(isJavaMethod));
        if (typeDrawing.equals(uploadTempHeaderDrawing)) {
            binders.putLocal("xDLocation", area);
            binders.putLocal("xDPlatform", platform);
            binders.putLocal("xDDisciplineCode", disciplineCode);
            binders.putLocal("xDDocTypeCode", documentType);
            binders.putLocal("xRunningNumber", runningNumber);
            binders.putLocal("primaryFile", folderSource);
            binders.putLocal("primaryFile:path", folderSource);
            binders.putLocal("xDocName", documentName);
            binders.putLocal("xDocNumber", documentNumber);
            binders.putLocal("dDocTitle", documentTitle);
            binders.putLocal("xDocPurpose", documentPurpose);
            binders.putLocal("xStatus", status);
            binders.putLocal("xSheetNumber", sheetNumber);
            binders.putLocal("xMOCNumber", MOCNo);
            binders.putLocal("xOldDocNumber", oldDocNo);
            binders.putLocal("xAuthor", author);
            binders.putLocal("xAuthorOrganization", authorOrganization);
            binders.putLocal("xAuthorGroupName", authorGroupName);
            binders.putLocal("xCompany", company);
            binders.putLocal("xProjectName", projectName);
            binders.putLocal("xContractor", contractor);
            binders.putLocal("xContracNumber", contractNo);
            binders.putLocal("xWONumber", WONo);
            binders.putLocal("xDocModel", docModel);
            binders.putLocal("xComments", description);
            binders.putLocal("xRevision", revision);
            binders.putLocal("dDocAccount", "");
            binders.putLocal("destinationPath", destinationPath);
            binders.putLocal("fFolderFile", documentName);
            //binders.putLocal("dSecurityGroup", "PHEDrawing");// set document security group
            binders.putLocal("dSecurityGroup", securityGroup);
            binders.putLocal("createPrimaryMetaFile", "0");
            Validator validator = new Validator();
            if (!validator.isExistFile(folderSource)) {
                return "executeFailed:primaryFile";
            }
        } else if (typeDrawing.equalsIgnoreCase(uploadTempHeaderNonDrawing)) {
            binders.putLocal("xNdLocation", area);
            binders.putLocal("xNdDisciplineCode", disciplineCode);
            binders.putLocal("xNdDocTypeCode", documentType);
            binders.putLocal("xRunningNumber", runningNumber);
            binders.putLocal("primaryFile", folderSource);
            binders.putLocal("primaryFile:path", folderSource);
            binders.putLocal("xDocName", documentName);
            binders.putLocal("xDocNumber", documentNumber);
            binders.putLocal("dDocTitle", documentTitle);
            binders.putLocal("xDocPurpose", documentPurpose);
            binders.putLocal("xStatus", status);
            binders.putLocal("xOldDocNumber", oldDocNo);
            binders.putLocal("xAuthor", author);
            binders.putLocal("xAuthorOrganization", authorOrganization);
            binders.putLocal("xAuthorGroupName", authorGroupName);
            binders.putLocal("xCompany", company);
            binders.putLocal("xProjectName", projectName);
            binders.putLocal("xContractor", contractor);
            binders.putLocal("xContracNumber", contractNo);
            binders.putLocal("xWONumber", WONo);
            binders.putLocal("xRevision", revision);
            binders.putLocal("dDocAccount", "");
            binders.putLocal("destinationPath", destinationPath);
            binders.putLocal("xRetentionPeriod", retentionPeriod);
            binders.putLocal("dSecurityGroup", securityGroup);
            if (activePassive.equalsIgnoreCase("Active")) {
                //Date date = new Date();
                String tanggal = retentionDate.substring(0, retentionDate.indexOf("/"));
                String sisa = retentionDate.substring(retentionDate.indexOf("/") + 1);
                String bulan = sisa.substring(0, sisa.indexOf("/"));
                sisa = sisa.substring(retentionDate.indexOf("/") + 1);
                String tahun = sisa;
                retentionDate = bulan + "/" + tanggal + "/" + tahun;
                DateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm a");
                try {
                    binders.putLocal("xRetentionDate", sdf.format(sdf.parse(retentionDate + " 11:59 PM")));
                    binder.setFieldType("xRetentionDate", "Date");
                } catch (Exception ex) {
                    Log.error("PHE:parsingDate:ex" + ex.getMessage());
                }
            }
        }
        DateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");
        Date date = new Date();
        if (dDocName.length() == 0) {
            binders.putLocal("dDocName", "EDMS" + dateFormat.format(date) + runningNumber + "B");
        } else {
            binders.putLocal("dDocName", dDocName);
            binders.putLocal("dID", dID);
        }
        binders.putLocal("isUpdate", "true");
        binders.putLocal("dDocType", docClasification);
        binders.putLocal("isCustom", "true");
        try {
            intradocservice.Util util = new intradocservice.Util();
            util.setUserLogin(userLogin);
            returnFunction = util.UpdateCheckin(binders, dColl.getSystemWorkspace());
        } catch (Exception ex) {
            DataBinder binderUndoCheckout = new DataBinder();
            binderUndoCheckout.putLocal("dDocName", dDocName);
            executeUndoCheckoutByName(binderUndoCheckout, workspace, userLogin);
            return "executeFailed";
        }
        return returnFunction;
    }

    public String executeUpdateRevision(
            DataBinder binder,
            String typeDrawing,
            String indexRow,
            String area,
            String platform,
            String disciplineCode,
            String documentType,
            String runningNumber,
            String sheetNumber,
            String documentTitle,
            String oldDocNo,
            String projectName,
            String projectYear,
            String ownerOrganization,
            String ownerName,
            String author,
            String remarks,
            String userLogin,
            String docTitle,
            String securityGroup,
            String docClasification,
            String sheetName,
            String error,
            String documentName,
            String documentNumber,
            String authorOrganization,
            String company,
            String retentionPeriod,
            String buAssetFunction,
            String destinationPath,
            String folderSource,
            String MOCNo,
            String authorGroupName,
            String contractor,
            String contractNo,
            String WONo,
            String revision,
            String status,
            String documentPurpose,
            String dDocName,
            String dID,
            String activePassive,
            String retentionDate,
            String docModel,
            String description,
            boolean isJavaMethod)
            throws DataException, ServiceException {
        String returnFunction = "";
        DataCollection dColl = new DataCollection();
        Workspace workspace = dColl.getSystemWorkspace();

        /* IF CHECKOUT_BY_NAME*/
        DataBinder binderCheckout = new DataBinder();
        binderCheckout.putLocal("dDocName", dDocName);
        returnFunction = executeCheckoutByName(binderCheckout, workspace, userLogin);
        
        
        String fParentGUID = GetFParentGUID(workspace, dDocName);
        String checkoutAdditionalParams = "&fParentGUID=" + GetFParentGUID(workspace, dDocName);
        returnFunction = executeCheckout(dID, dDocName, documentTitle, fParentGUID, checkoutAdditionalParams, userLogin);
        
        Log.info("<== RETURN FUNCTION executeCheckoutByName: |" + returnFunction + "| ==> ");
        if (returnFunction.equals("executeFailed")) {
        	Log.info("<-- RETURN FUNCTION executeFailed -->");
            returnFunction = "executeFailed:checkoutFailed";
            
            DataBinder binderUndoCheckout = new DataBinder();
            binderUndoCheckout.putLocal("dDocName", dDocName);
            executeUndoCheckoutByName(binderUndoCheckout, workspace, userLogin);
            
            return returnFunction;
        } else {
        	Log.info("<-- RETURN FUNCTION Maju for Checkin -->");
            DataBinder binders = new DataBinder();
            binders.putLocal("isJavaMethod", String.valueOf(isJavaMethod));
            if (typeDrawing.equals(uploadTempHeaderDrawing)) {
                binders.putLocal("xDLocation", area);
                binders.putLocal("xDPlatform", platform);
                binders.putLocal("xDDisciplineCode", disciplineCode);
                binders.putLocal("xDDocTypeCode", documentType);
                binders.putLocal("xRunningNumber", runningNumber);
                binders.putLocal("primaryFile", folderSource);
                binders.putLocal("primaryFile:path", folderSource);
                binders.putLocal("xDocName", documentName);
                binders.putLocal("xDocNumber", documentNumber);
                binders.putLocal("dDocTitle", documentTitle);
                binders.putLocal("xDocPurpose", documentPurpose);
                binders.putLocal("xSheetNumber", sheetNumber);
                binders.putLocal("xStatus", status);
                binders.putLocal("xMOCNumber", MOCNo);
                binders.putLocal("xOldDocNumber", oldDocNo);;
                binders.putLocal("xAuthor", author);
                binders.putLocal("xAuthorOrganization", authorOrganization);
                binders.putLocal("xAuthorGroupName", authorGroupName);
                binders.putLocal("xCompany", company);
                binders.putLocal("xProjectName", projectName);
                binders.putLocal("xContractor", contractor);
                binders.putLocal("xContracNumber", contractNo);
                binders.putLocal("xWONumber", WONo);
                binders.putLocal("xDocModel", docModel);
                binders.putLocal("xComments", description);
                binders.putLocal("xRevision", revision);
                binders.putLocal("dRevLabel", revision);
                binders.putLocal("dDocAccount", "");
                binders.putLocal("destinationPath", destinationPath);
                binders.putLocal("fFolderFile", documentName);
                /*
                if (documentPurpose.equalsIgnoreCase("Published")) {
                    binders.putLocal("dSecurityGroup", "PHEDrawing");
                } else {
                    binders.putLocal("dSecurityGroup", "PHE_TDC");
                }
                */
                binders.putLocal("dSecurityGroup", securityGroup);
            } 
            
            if (typeDrawing.equals(uploadTempHeaderNonDrawing)) {
                binders.putLocal("xNdLocation", area);
                binders.putLocal("xNdDisciplineCode", disciplineCode);
                binders.putLocal("xNdDocTypeCode", documentType);
                binders.putLocal("xRunningNumber", runningNumber);
                binders.putLocal("primaryFile", folderSource);
                binders.putLocal("primaryFile:path", folderSource);
                binders.putLocal("xDocName", documentName);
                binders.putLocal("xDocNumber", documentNumber);
                binders.putLocal("dDocTitle", documentTitle);
                binders.putLocal("xDocPurpose", documentPurpose);
                binders.putLocal("xStatus", status);
                binders.putLocal("xOldDocNumber", oldDocNo);;
                binders.putLocal("xAuthor", author);
                binders.putLocal("xAuthorOrganization", authorOrganization);
                binders.putLocal("xAuthorGroupName", authorGroupName);
                binders.putLocal("xCompany", company);
                binders.putLocal("xProjectName", projectName);
                binders.putLocal("xContractor", contractor);
                binders.putLocal("xContracNumber", contractNo);
                binders.putLocal("xWONumber", WONo);
                binders.putLocal("xRevision", revision);
                binders.putLocal("dRevLabel", revision);
                binders.putLocal("dDocAccount", "");
                binders.putLocal("destinationPath", destinationPath);
                binders.putLocal("xRetentionPeriod", retentionPeriod);

                if (activePassive.equalsIgnoreCase("ACTIVE")) {
                    //Date date = new Date();
                    String tanggal = retentionDate.substring(0, retentionDate.indexOf("/"));
                    String sisa = retentionDate.substring(retentionDate.indexOf("/") + 1);
                    String bulan = sisa.substring(0, sisa.indexOf("/"));
                    sisa = sisa.substring(retentionDate.indexOf("/") + 1);
                    String tahun = sisa;
                    retentionDate = bulan + "/" + tanggal + "/" + tahun;
                    DateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm a");
                    try {
                        binders.putLocal("xRetentionDate", sdf.format(sdf.parse(retentionDate + " 11:59 PM")));
                        binder.setFieldType("xRetentionDate", "Date");
                    } catch (Exception ex) {
                        Log.error("PHE:parsingDate:ex" + ex.getMessage());
                    }
                }
                binders.putLocal("fFolderFile", documentName);
                /*
                if (documentPurpose.equalsIgnoreCase("Published")) {
                    binders.putLocal("dSecurityGroup", "PHENonDrawing");
                } else {
                    binders.putLocal("dSecurityGroup", "PHE_TDC");
                }
                */
                binders.putLocal("dSecurityGroup", securityGroup);
            }

            DateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");
            Date date = new Date();

            Log.info("PHE:executeUpdateRevision:dDocName:" + dDocName);
            if (dDocName.length() == 0) {
                binders.putLocal("dDocName", "EDMS" + dateFormat.format(date) + runningNumber + "B");
            } else {
                binders.putLocal("dDocName", dDocName);
            }
            binders.putLocal("createPrimaryMetaFile", "0");
            binders.putLocal("isUpdate", "true");
            binders.putLocal("dDocAuthor", userLogin);
            binders.putLocal("dDocType", docClasification);
            binders.putLocal("isCustom", "true");
            try {
                try {
                    if (documentName.indexOf(" ") >= 0) {
                        documentName = documentName.substring(0, documentName.indexOf(" "));
                    } else if (documentName.indexOf(".") >= 0) {
                        documentName = documentName.substring(0, documentName.indexOf("."));
                    }

                    binders.putLocal("xDocName", documentName);
                } catch (Exception ex) {
                }
                Log.info("PHE:executeCheckinUniversalForRevision***:" + binders);
                returnFunction = executeCheckinUniversalForRevision(binders, dColl.getSystemWorkspace(), userLogin);
                if (returnFunction.length() == 0) {
                    returnFunction = binders.getLocal("dID") + ";" + binders.getLocal("dDocName");
                }
            } catch (Exception ex) {
                DataBinder binderUndoCheckout = new DataBinder();
                binderUndoCheckout.putLocal("dDocName", dDocName);
                executeUndoCheckoutByName(binderUndoCheckout, workspace, userLogin);
				return "executeFailed";
            }
            return returnFunction;
        }
    }

    public String executeCheckinUniversal(
            DataBinder binder,
            String typeDrawing,
            String indexRow,
            String area,
            String platform,
            String disciplineCode,
            String documentType,
            String runningNumber,
            String sheetNumber,
            String documentTitle,
            String oldDocNo,
            String projectName,
            String projectYear,
            String ownerOrganization,
            String ownerName,
            String author,
            String remarks,
            String userLogin,
            String docTitle,
            String securityGroup,
            String docClasification,
            String sheetName,
            String error,
            String documentName,
            String documentNumber,
            String authorOrganization,
            String company,
            String retentionPeriod,
            String buAssetFunction,
            String destinationPath,
            String folderSource,
            String MOCNo,
            String authorGroupName,
            String contractor,
            String contractNo,
            String WONo,
            String revision,
            String status,
            String documentPurpose,
            String activePassive,
            String retentionDate,
            String docModel,
            String description,
            boolean isJavaMethod)
            throws DataException, ServiceException {
        String returnFunction = "";
        DataCollection dColl = new DataCollection();

        Validator validator = new Validator();
        DataBinder binders = new DataBinder();
        binders.putLocal("IdcService", "CHECKIN_UNIVERSAL");
        binders.putLocal("isJavaMethod", String.valueOf(isJavaMethod));
        if (typeDrawing.equals(uploadTempHeaderMasterlistDrawing)) {
            binders.putLocal("xDLocation", area);
            binders.putLocal("xDPlatform", platform);
            binders.putLocal("xDDisciplineCode", disciplineCode);
            binders.putLocal("xDDocTypeCode", documentType);
            binders.putLocal("xRunningNumber", runningNumber);
            binders.putLocal("xSheetNumber", sheetNumber);
            binders.putLocal("dDocTitle", documentTitle);
            binders.putLocal("xOldDocNumber", oldDocNo);
            binders.putLocal("xProjectName", projectName);
            binders.putLocal("xProjectYear", projectYear);
            binders.putLocal("xAuthorOrganization", ownerOrganization);
            binders.putLocal("xAuthor", ownerName);
            binders.putLocal("xRemarks", remarks);
            binders.putLocal("xStatus", "Registered");
            binders.putLocal("dDocAccount", "");
            binders.putLocal("xMOCNumber", MOCNo);
            binders.putLocal("xDocNumber", area + "-" + platform + "-" + disciplineCode + "-" + documentType + "-" + runningNumber);
            binders.putLocal("createPrimaryMetaFile", "1");
            binders.putLocal("xCompany", company);
        } else if (typeDrawing.equals(uploadTempHeaderMasterlistNonDrawing)) {
            binders.putLocal("xNdLocation", area);
            binders.putLocal("xNdPlatform", platform);
            binders.putLocal("xDocNumber", documentNumber);
            binders.putLocal("xNdDisciplineCode", disciplineCode);
            binders.putLocal("xNdDocTypeCode", documentType);
            binders.putLocal("xRunningNumber", runningNumber);
            binders.putLocal("xSheetNumber", sheetNumber);
            binders.putLocal("dDocTitle", documentTitle);
            binders.putLocal("xOldDocNumber", oldDocNo);
            binders.putLocal("xProjectName", projectName);
            binders.putLocal("xProjectYear", projectYear);
            binders.putLocal("xAuthorOrganization", ownerOrganization);
            binders.putLocal("xAuthor", ownerName);
            binders.putLocal("xAuthor", author);
            binders.putLocal("xRemarks", remarks);
            binders.putLocal("dDocAccount", "");
            binders.putLocal("xStatus", "Registered");
            binders.putLocal("createPrimaryMetaFile", "1");
            binders.putLocal("xDocNumber", area + "-" + disciplineCode + "-" + documentType + "-" + runningNumber);
            binders.putLocal("xCompany", company);
        } else if (typeDrawing.equals(uploadTempHeaderGeneral)) {
        	//Users users = new Users();
            binders.putLocal("primaryFile", folderSource);
            binders.putLocal("primaryFile:path", folderSource);
            if (!validator.isExistFile(folderSource)) {
                return "executeFailed:primaryFile";
            }
            binders.putLocal("xDocName", documentName);
            binders.putLocal("xDocNumber", documentNumber);
            binders.putLocal("xAuthor", author);
            binders.putLocal("xAuthorOrganization", authorOrganization);
            binders.putLocal("xProjectName", projectName);
            binders.putLocal("xCompany", company);
            binders.putLocal("xOldDocNumber", oldDocNo);
            binders.putLocal("xRetentionPeriod", retentionPeriod);
            binders.putLocal("xBUAssetFunction", buAssetFunction);
            binders.putLocal("dDocTitle", documentTitle);
            binders.putLocal("dDocAccount", "");
            binders.putLocal("destinationPath", destinationPath);
            binders.putLocal("xDocPurpose", "Published");
            binders.putLocal("xStatus", "Published");
            binders.putLocal("dDocType", documentType);
            binders.putLocal("xDocModel", docModel);
            binders.putLocal("xComments", description);
        } else if (typeDrawing.equals(uploadTempHeaderPublic)) {
        	//Users users = new Users();
            binders.putLocal("primaryFile", folderSource);
            binders.putLocal("primaryFile:path", folderSource);
            if (!validator.isExistFile(folderSource)) {
                return "executeFailed:primaryFile";
            }
            binders.putLocal("xDocName", documentName);
            binders.putLocal("xDocNumber", documentNumber);
            binders.putLocal("xAuthor", author);
            binders.putLocal("xAuthorOrganization", authorOrganization);
            binders.putLocal("xProjectName", projectName);
            binders.putLocal("xCompany", company);
            binders.putLocal("xOldDocNumber", oldDocNo);
            binders.putLocal("xRetentionPeriod", retentionPeriod);
            binders.putLocal("xBUAssetFunction", buAssetFunction);
            binders.putLocal("dDocTitle", documentTitle);
            binders.putLocal("dDocAccount", "");
            binders.putLocal("destinationPath", destinationPath);
            binders.putLocal("xDocPurpose", "Published");
            binders.putLocal("xStatus", "Published");
            binders.putLocal("dDocType", documentType);
            binders.putLocal("xDocModel", docModel);
            binders.putLocal("xComments", description);
        } else if (typeDrawing.equals(uploadTempHeaderDrawing)) {
            binders.putLocal("xDLocation", area);
            binders.putLocal("xDPlatform", platform);
            binders.putLocal("xDDisciplineCode", disciplineCode);
            binders.putLocal("xDDocTypeCode", documentType);
            try {
                if (runningNumber.indexOf(" ") >= 0) {
                    runningNumber = runningNumber.substring(0, runningNumber.indexOf(" "));
                }
            } catch (Exception ex) {
            }
            binders.putLocal("xRunningNumber", runningNumber);
            binders.putLocal("primaryFile", folderSource);
            binders.putLocal("primaryFile:path", folderSource);
            if (!validator.isExistFile(folderSource)) {
                return "executeFailed:primaryFile";
            }
            binders.putLocal("xDocName", documentName);
            binders.putLocal("xDocNumber", documentNumber);
            binders.putLocal("dDocTitle", documentTitle);
            binders.putLocal("xDocPurpose", documentPurpose);
            binders.putLocal("xStatus", status);
            binders.putLocal("xMOCNumber", MOCNo);
            binders.putLocal("xOldDocNumber", oldDocNo);
            binders.putLocal("xAuthor", author);
            binders.putLocal("xAuthorOrganization", authorOrganization);
            binders.putLocal("xAuthorGroupName", authorGroupName);
            binders.putLocal("xCompany", company);
            binders.putLocal("xProjectName", projectName);
            binders.putLocal("xContractor", contractor);
            binders.putLocal("xContracNumber", contractNo);
            binders.putLocal("xWONumber", WONo);
            binders.putLocal("xDocModel", docModel);
            binders.putLocal("xComments", description);
            binders.putLocal("xRevision", revision);
            binders.putLocal("dDocAccount", "");
            binders.putLocal("xSheetNumber", sheetNumber);
            binders.putLocal("destinationPath", destinationPath);
        } else if (typeDrawing.equals(uploadTempHeaderNonDrawing)) {
            binders.putLocal("xNdLocation", area);
            binders.putLocal("xNdDisciplineCode", disciplineCode);
            binders.putLocal("xNdDocTypeCode", documentType);
            try {
                if (runningNumber.indexOf(" ") >= 0) {
                    runningNumber = runningNumber.substring(0, runningNumber.indexOf(" "));
                }
            } catch (Exception ex) {
            }
            binders.putLocal("xRunningNumber", runningNumber);
            binders.putLocal("primaryFile", folderSource);
            binders.putLocal("primaryFile:path", folderSource);
            binders.putLocal("xDocName", documentName);
            binders.putLocal("xDocNumber", documentNumber);
            binders.putLocal("dDocTitle", documentTitle);
            binders.putLocal("xDocPurpose", documentPurpose);
            binders.putLocal("xStatus", status);
            binders.putLocal("xOldDocNumber", oldDocNo);
            binders.putLocal("xAuthor", author);
            binders.putLocal("xAuthorOrganization", authorOrganization);
            binders.putLocal("xAuthorGroupName", authorGroupName);
            binders.putLocal("xCompany", company);
            binders.putLocal("xProjectName", projectName);
            binders.putLocal("xContractor", contractor);
            binders.putLocal("xContracNumber", contractNo);
            binders.putLocal("xWONumber", WONo);
            binders.putLocal("xRevision", revision);
            binders.putLocal("dDocAccount", "");
            binders.putLocal("destinationPath", destinationPath);
            binders.putLocal("xRetentionPeriod", retentionPeriod);
            if (activePassive.equalsIgnoreCase("Active")) {
                //Date date = new Date();
                String tanggal = retentionDate.substring(0, retentionDate.indexOf("/"));
                String sisa = retentionDate.substring(retentionDate.indexOf("/") + 1);
                String bulan = sisa.substring(0, sisa.indexOf("/"));
                sisa = sisa.substring(retentionDate.indexOf("/") + 1);
                String tahun = sisa;
                retentionDate = bulan + "/" + tanggal + "/" + tahun;
                DateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm a");
                try {
                    binders.putLocal("xRetentionDate", sdf.format(sdf.parse(retentionDate + " 11:59 PM")));
                    binder.setFieldType("xRetentionDate", "Date");
                } catch (Exception ex) {
                    Log.error("PHE:parsingDate:ex" + ex.getMessage());
                }
            }
        }

        DateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");
        Date date = new Date();

        try {
            if (runningNumber.indexOf(" ") >= 0) {
                runningNumber = runningNumber.substring(0, runningNumber.indexOf(" "));
            }
        } catch (Exception ex) {
        }
        binders.putLocal("dDocName", "EDMS" + dateFormat.format(date) + runningNumber + "B");
        binders.putLocal("dSecurityGroup", securityGroup);
        binders.putLocal("dDocAuthor", userLogin);
        binders.putLocal("dDocType", docClasification);
        binders.putLocal("isCustom", "true");
        try {
            returnFunction = dColl.executeService(binders, userLogin, true);
            if (returnFunction.length() == 0) {
                returnFunction = binders.getLocal("dID") + ";" + binders.getLocal("dDocName");
            }
        } catch (Exception ex) {
            return "executeFailed";
        }
        return returnFunction;
    }

    public String executeUpdateDocInfo(
            DataBinder binder,
            String typeDrawing,
            String indexRow,
            String area,
            String platform,
            String disciplineCode,
            String documentType,
            String runningNumber,
            String sheetNumber,
            String documentTitle,
            String oldDocNo,
            String projectName,
            String projectYear,
            String ownerOrganization,
            String ownerName,
            String author,
            String remarks,
            String description,
            String userLogin,
            String docTitle,
            String securityGroup,
            String docClasification,
            String sheetName,
            String error,
            String documentName,
            String documentNumber,
            String authorOrganization,
            String company,
            String retentionPeriod,
            String buAssetFunction,
            String destinationPath,
            String folderSource,
            String MOCNo,
            String authorGroupName,
            String contractor,
            String contractNo,
            String WONo,
            String revision,
            String status,
            String documentPurpose,
            String activePassive,
            String retentionDate,
            boolean isJavaMethod)
            throws DataException, ServiceException {
        String returnFunction = "";
        DataCollection dColl = new DataCollection();
        DatabaseValue dValue = new DatabaseValue();
        DataBinder binders = new DataBinder();
        binders.putLocal("IdcService", "UPDATE_DOCINFO");
        binders.putLocal("isJavaMethod", String.valueOf(isJavaMethod));
        binders.putLocal("isUpdate", "true");
        binders.putLocal("dID", dValue.getDIDDocumentNumberDeteledStatus(documentNumber, sheetNumber));
        if (typeDrawing.equals(uploadTempHeaderMasterlistDrawing)) {
            binders.putLocal("xDLocation", area);
            binders.putLocal("xDPlatform", platform);
            binders.putLocal("xDDisciplineCode", disciplineCode);
            binders.putLocal("xDDocTypeCode", documentType);
            binders.putLocal("xRunningNumber", runningNumber);
            binders.putLocal("xSheetNumber", sheetNumber);
            binders.putLocal("dDocTitle", documentTitle);
            binders.putLocal("xOldDocNumber", oldDocNo);
            binders.putLocal("xProjectName", projectName);
            binders.putLocal("xProjectYear", projectYear);
            binders.putLocal("xAuthorOrganization", ownerOrganization);
            binders.putLocal("xAuthor", ownerName);
            binders.putLocal("xRemarks", remarks);
            binders.putLocal("xComments", description);
            binders.putLocal("xStatus", "Registered");
            binders.putLocal("dDocAccount", "");
            binders.putLocal("xMOCNumber", MOCNo);
            binders.putLocal("createPrimaryMetaFile", "1");
        } else if (typeDrawing.equals(uploadTempHeaderMasterlistNonDrawing)) {
            binders.putLocal("xNdLocation", area);
            binders.putLocal("xNdPlatform", platform);
            binders.putLocal("xNdDisciplineCode", disciplineCode);
            binders.putLocal("xNdDocTypeCode", documentType);
            binders.putLocal("xRunningNumber", runningNumber);
            binders.putLocal("xSheetNumber", sheetNumber);
            binders.putLocal("dDocTitle", documentTitle);
            binders.putLocal("xOldDocNumber", oldDocNo);
            binders.putLocal("xProjectName", projectName);
            binders.putLocal("xProjectYear", projectYear);
            binders.putLocal("xAuthorOrganization", ownerOrganization);
            binders.putLocal("xAuthor", ownerName);
            binders.putLocal("xAuthor", author);
            binders.putLocal("xRemarks", remarks);
            binders.putLocal("xComments", description);
            binders.putLocal("dDocAccount", "");
            binders.putLocal("xStatus", "REGISTERED");
            binders.putLocal("createPrimaryMetaFile", "1");
        }
        binders.putLocal("dSecurityGroup", securityGroup);
        binders.putLocal("dDocType", docClasification);
        binders.putLocal("isCustom", "true");
        try {
            returnFunction = dColl.executeService(binders, userLogin, true);
        } catch (Exception ex) {
            Log.error("PHE:executeUpdateDocInfo:indexRow:" + indexRow + ":ex:" + ex.getMessage().toString());
            return "executeFailed";
        }
        return returnFunction;
    }

    public String executeUpdateDocInfoJustFParentGUID(
            DataBinder binder,
            String indexRow,
            String userLogin,
            boolean isJavaMethod)
            throws DataException, ServiceException {
        String returnFunction = "";
        DataCollection dColl = new DataCollection();
        //DatabaseValue dValue = new DatabaseValue();

        binder.putLocal("IdcService", "UPDATE_DOCINFO");
        binder.putLocal("isJavaMethod", String.valueOf(isJavaMethod));
        binder.putLocal("isUpdate", "true");
        binder.putLocal("isCustom", "true");
        try {
            returnFunction = dColl.executeService(binder, userLogin, true);
        } catch (Exception ex) {
            Log.error("PHE:executeUpdateDocInfoJustFParentGUID:indexRow:" + indexRow + ":ex:" + ex.getMessage().toString());
            return "executeFailed";
        }
        return returnFunction;
    }

    public String executeUpdateDocInfoJustxStatus(
            DataBinder binder,
            String indexRow,
            String dDocName,
            String dID,
            String xStatus,
            String typeDrawing,
            String userLogin,
            boolean isJavaMethod)
            throws DataException, ServiceException {
        String returnFunction = "";
        DataCollection dColl = new DataCollection();
        //DatabaseValue dValue = new DatabaseValue();
        DataBinder binders = new DataBinder();
        binders.putLocal("IdcService", "UPDATE_DOCINFO");
        binders.putLocal("isJavaMethod", String.valueOf(isJavaMethod));
        binders.putLocal("isUpdate", "true");
        binders.putLocal("dID", dID);
        binders.putLocal("dDocName", dDocName);
        binders.putLocal("xStatus", xStatus);
        binders.putLocal("isCustom", "true");
        try {
            returnFunction = dColl.executeService(binders, userLogin, true);
        } catch (Exception ex) {
            return "executeFailed:updateStatus";
        }
        return returnFunction;
    }

    public String executeUpdateDocInfoJustObjectID(
            DataBinder binder,
            String indexRow,
            String dDocName,
            String dID,
            String typeDrawing,
            String userLogin,
            boolean isJavaMethod)
            throws DataException, ServiceException {
        String returnFunction = "";
        DataCollection dColl = new DataCollection();
        DataBinder binders = new DataBinder();
        binders.putLocal("IdcService", "UPDATE_DOCINFO");
        binders.putLocal("isJavaMethod", String.valueOf(isJavaMethod));
        binders.putLocal("isUpdate", "true");
        binders.putLocal("isCustom", "true");
        binders.putLocal("dID", dID);
        if (dDocName.length() > 0) {
            binders.putLocal("dDocName", dDocName);
        }
        binders.putLocal("xRObjectId", dID);
        try {
            returnFunction = dColl.executeService(binders, userLogin, true);
        } catch (Exception ex) {
            Log.error("PHE:executeUpdateDocInfoJustObjectID:indexRow:" + indexRow + ":ex:" + ex.getMessage().toString());
            return "executeFailed:updateRObjectID";
        }
        return returnFunction;
    }

    public String executeCheckout(
            String dID,
            String dDocName,
            String dDocTitle,
            String fParentGUID,
            String checkoutAdditionalParams,
            String userLogin) {
        String returnFunction = "";
        DataCollection dColl = new DataCollection();
        DataBinder binders = new DataBinder();
        binders.putLocal("IdcService", "CHECKOUT");
        binders.putLocal("dID ", dID);
        binders.putLocal("dDocName", dDocName);
        binders.putLocal("dDocTitle", dDocTitle);
        binders.putLocal("fParentGUID", fParentGUID);
        binders.putLocal("checkoutAdditionalParams", checkoutAdditionalParams);
        try {
            dColl.executeService(binders, userLogin, true);
        } catch (Exception ex) {
            Log.error("PHE:executeCheckoutFailed:ex:" + ex.getMessage().toString());
            return "executeFailed";
        }
        return returnFunction;
    }

    public String executeCheckin(
            String filepath,
            String typeDrawing,
            String userLogin,
            String securityGroup,
            String dDocTitle,
            String docType,
            Date date) {
        String returnFunction = "";
        DataCollection dColl = new DataCollection();
        DataBinder binders = new DataBinder();
        binders.putLocal("IdcService", "CHECKIN_UNIVERSAL");
        DateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");
        binders.putLocal("dDocName", "BLOG_" + dateFormat.format(date));
        binders.putLocal("dSecurityGroup", securityGroup);
        binders.putLocal("dDocTitle", dDocTitle);
        binders.putLocal("primaryFile", filepath);
        binders.putLocal("dDocAccount", "");
        binders.putLocal("dDocAuthor", userLogin);
        binders.putLocal("dDocType", docType);
        binders.putLocal("isCustom", "true");
        try {
            dColl.executeService(binders, userLogin, true);
        } catch (Exception ex) {
            Log.error("PHE:executeFailed:ex:" + ex.getMessage().toString());
            return "executeFailed";
        }
        return returnFunction;
    }

    public String deleteDoc(
            String dDocName, String userLogin, Workspace workpsace) {
        String returnFunction = "";
        DataCollection dColl = new DataCollection();
        DataBinder binders = new DataBinder();
        binders.putLocal("IdcService", "DELETE_DOC");
        binders.putLocal("dDocName", dDocName);
        try {
            returnFunction = dColl.executeService(binders, userLogin, true);
        } catch (Exception ex) {
            Log.error("PHE:deleteDoc:ex:" + ex.getMessage().toString());
            return "executeFailed";
        }
        return returnFunction;
    }

    public String checkinSimilar(String userLogin, ResultSet rs, String xStatus) {
        DataCollection dColl = new DataCollection();
        DataBinder binderCheckin = new DataBinder();
        binderCheckin.putLocal("IdcService", "CHECKIN_UNIVERSAL");
        String dDocType = "";
        if (rs.getStringValueByName("dDocType").toUpperCase().indexOf("NONDRAWING") >= 0) {
            dDocType = "MasterListNonDrawing";
        } else {
            dDocType = "MasterListDrawing";
        }
        binderCheckin.putLocal("dDocType", dDocType);
        binderCheckin.putLocal("dDocTitle", rs.getStringValueByName("dDocTitle"));
        binderCheckin.putLocal("dDocAuthor", rs.getStringValueByName("dDocAuthor"));
        binderCheckin.putLocal("dSecurityGroup", rs.getStringValueByName("dSecurityGroup"));
        binderCheckin.putLocal("dDocAccount", rs.getStringValueByName("dDocAccount"));
        binderCheckin.putLocal("xProjectYear", rs.getStringValueByName("xProjectYear"));
        binderCheckin.putLocal("xSheetNumber", rs.getStringValueByName("xSheetNumber"));
        binderCheckin.putLocal("xRunningNumber", rs.getStringValueByName("xRunningNumber"));
        binderCheckin.putLocal("xOldDocNumber", rs.getStringValueByName("xOldDocNumber"));
        binderCheckin.putLocal("xAuthorOrganization", rs.getStringValueByName("xAuthorOrganization"));
        binderCheckin.putLocal("xAuthor", rs.getStringValueByName("xAuthor"));
        binderCheckin.putLocal("xRemarks", rs.getStringValueByName("xRemarks"));
        binderCheckin.putLocal("xAuthor", rs.getStringValueByName("xAuthor"));
        binderCheckin.putLocal("xNdLocation", rs.getStringValueByName("xNdLocation"));
        binderCheckin.putLocal("xDLocation", rs.getStringValueByName("xDLocation"));
        binderCheckin.putLocal("xDPlatform", rs.getStringValueByName("xDPlatform"));
        binderCheckin.putLocal("xNdDisciplineCode", rs.getStringValueByName("xNdDisciplineCode"));
        binderCheckin.putLocal("xDDisciplineCode", rs.getStringValueByName("xDDisciplineCode"));
        binderCheckin.putLocal("xNdDocTypeCode", rs.getStringValueByName("xNdDocTypeCode"));
        binderCheckin.putLocal("xDDocTypeCode", rs.getStringValueByName("xDDocTypeCode"));
        binderCheckin.putLocal("dDocOwner", rs.getStringValueByName("dDocOwner"));
        binderCheckin.putLocal("dDocName", rs.getStringValueByName("dDocName") + "D");
        binderCheckin.putLocal("xDocNumber", rs.getStringValueByName("xDocNumber"));
        binderCheckin.putLocal("xStatus", xStatus);
        binderCheckin.putLocal("createPrimaryMetaFile", "1");
        try {
            dColl.executeService(binderCheckin, userLogin, true);
            return rs.getStringValueByName("dDocName") + "D";
        } catch (DataException ex) {
            Log.error("PHE:checkinSimilar:ex:" + ex.getMessage());
            return "failedExecute";
        } catch (ServiceException ex) {
            Log.error("PHE:checkinSimilar:ex:" + ex.getMessage());
            return "failedExecute";
        }
    }

    public boolean IsAlreadyExist(String docNumber, String revision, String docPuporse, Workspace ws) {
        DatabaseValue db = new DatabaseValue();
        String query;
        query = "SELECT count(XDOCNUMBER)  FROM DOCMETA DM  "
                + "INNER JOIN REVISIONS R ON DM.DID = R.DID "
                + "WHERE XDOCNUMBER = '" + docNumber + "' AND XDOCPURPOSE = '" + docPuporse + "' AND DM.XREVISION = '" + revision + "'";
        return db.GetResultQuery(query, ws) == "0" ? false : true;
    }

    public boolean IsLessDocRevision(String docNumber, String revision, String docPuporse, Workspace ws) {
        DatabaseValue db = new DatabaseValue();
        String query;
        query = "SELECT count(XDOCNUMBER)  FROM DOCMETA DM  "
                + "INNER JOIN REVISIONS R ON DM.DID = R.DID "
                + "WHERE XDOCNUMBER = '" + docNumber + "' AND XDOCPURPOSE = '" + docPuporse + "' AND DM.XREVISION > " + revision + "";
        return db.GetResultQuery(query, ws) == "0" ? false : true;
    }

    public String GetTDCID(String docNumber, String revision, String docPuporse, Workspace ws) {
        DatabaseValue db = new DatabaseValue();
        String query;
        query = "SELECT dID  FROM DOCMETA DM  "
                + "INNER JOIN REVISIONS R ON DM.DID = R.DID "
                + "WHERE XDOCNUMBER = '" + docNumber + "' AND XDOCPURPOSE = '" + docPuporse + "' AND DM.XREVISION = '" + revision + "'";
        return db.GetResultQuery(query, ws) == null ? "" : db.GetResultQuery(query, ws);
    }

    public String GetDocIDByDocName(Workspace ws, String dDocName) {
        DatabaseValue db = new DatabaseValue();
        String query;
        query = "SELECT DID FROM REVISIONS "
                + "WHERE DDOCNAME LIKE '" + dDocName + "' "
                + "ORDER BY DID DESC";
        return db.GetResultQuery(query, ws) == null ? "0" : db.GetResultQuery(query, ws);
    }

    public String GetFParentGUID(Workspace ws, String dDocName) {
        DatabaseValue db = new DatabaseValue();
        String query;
        query = "SELECT FPARENTGUID FROM FOLDERFILES WHERE DDOCNAME = '"+dDocName+"'";
        return db.GetResultQuery(query, ws) == null ? "0" : db.GetResultQuery(query, ws);
    }

    public String executeCheckoutByName(DataBinder binder, Workspace ws, String userLogin) {
        DataCollection dc = new DataCollection();
        try {
            binder.putLocal("IdcService", "CHECKOUT_BY_NAME");
            Log.info("PHE:executeCheckoutByName:" + binder);
            return dc.executeService(binder, userLogin, true);
        } catch (Exception ex) {
            Log.info("PHE:CheckoutByName:ex" + ex.getMessage());
            return "executeFailed";
        }
    }

    public String executeUndoCheckoutByName(DataBinder binder, Workspace ws, String userLogin) {
        DataCollection dc = new DataCollection();
        try {
            binder.putLocal("IdcService", "UNDO_CHECKOUT_BY_NAME");
            return dc.executeService(binder, userLogin, true);
        } catch (Exception ex) {
            Log.error("PHE:CheckoutByName:ex" + ex.getMessage());
            return "executeFailed";
        }
    }

    public String executeCheckinByName(DataBinder binder, Workspace ws, String userLogin) {
        DataCollection dc = new DataCollection();
        try {
            binder.putLocal("IdcService", "CHECKIN_BYNAME");
            return dc.executeService(binder, userLogin, true);
        } catch (Exception ex) {
            Log.error("PHE:CheckoutByName:ex" + ex.getMessage());
            return "executeFailed";
        }
    }

    public String executeCheckinUniversalForRevision(DataBinder binder, Workspace ws, String userLogin) {
        DataCollection dc = new DataCollection();
        try {
            binder.putLocal("IdcService", "CHECKIN_UNIVERSAL");
        	//binder.putLocal("IdcService", "CHECKIN_SEL_SUB");
            Log.info("***PHE:executeCheckinUniversalForRevision:\n"
            		+ "*** USER LOGIN: " + userLogin + "\n"
            		+ binder);
            return dc.executeService(binder, userLogin, true);
        } catch (Exception ex) {
            Log.error("PHE:executeCheckinUniversalForRevision:ex" + ex.getMessage());
            return "executeFailed";
        }
    }
    
    public String executeUpdateDocInfoDIS(
            DataBinder binder,
            String typeDrawing,
            String indexRow,
            String area,
            String platform,
            String disciplineCode,
            String documentType,
            String runningNumber,
            String sheetNumber,
            String documentTitle,
            String oldDocNo,
            String projectName,
            String projectYear,
            String ownerOrganization,
            String ownerName,
            String author,
            String remarks,
            String userLogin,
            String docTitle,
            String securityGroup,
            String docClasification,
            String sheetName,
            String error,
            String documentName,
            String documentNumber,
            String authorOrganization,
            String company,
            String retentionPeriod,
            String buAssetFunction,
            String destinationPath,
            String folderSource,
            String MOCNo,
            String authorGroupName,
            String contractor,
            String contractNo,
            String WONo,
            String revision,
            String status,
            String documentPurpose,
            String activePassive,
            String retentionDate,
            String docModel,
            String description,
            boolean isJavaMethod)
            throws DataException, ServiceException {
        String returnFunction = "";
        DataCollection dColl = new DataCollection();
        DatabaseValue dValue = new DatabaseValue();
        //Validator validator = new Validator();
        DataBinder binders = new DataBinder();
        
        try {
            if (documentName.indexOf(" ") >= 0) {
            	documentName = documentName.substring(0, documentName.indexOf(" "));
            } else if (documentName.indexOf(".") >= 0) {
            	documentName = documentName.substring(0, documentName.indexOf("."));
            }
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        
        binders.putLocal("IdcService", "UPDATE_DOCINFO");
        binders.putLocal("isJavaMethod", String.valueOf(isJavaMethod));
        binders.putLocal("isUpdate", "true");
        binders.putLocal("isCustom", "true");
        binders.putLocal("dID", dValue.getDIDDocumentDIS(documentName, documentPurpose, docClasification, securityGroup));
        binders.putLocal("dDocName", dValue.getDDocNameDocumentDIS(documentName, documentPurpose, docClasification, securityGroup));
        if (typeDrawing.equals(uploadTempHeaderMasterlistDrawing)) {
            binders.putLocal("xDLocation", area);
            binders.putLocal("xDPlatform", platform);
            binders.putLocal("xDDisciplineCode", disciplineCode);
            binders.putLocal("xDDocTypeCode", documentType);
            binders.putLocal("xRunningNumber", runningNumber);
            binders.putLocal("xSheetNumber", sheetNumber);
            binders.putLocal("dDocTitle", documentTitle);
            binders.putLocal("xOldDocNumber", oldDocNo);
            binders.putLocal("xProjectName", projectName);
            binders.putLocal("xProjectYear", projectYear);
            binders.putLocal("xAuthorOrganization", ownerOrganization);
            binders.putLocal("xAuthor", ownerName);
            binders.putLocal("xRemarks", remarks);
            binders.putLocal("xStatus", "Registered");
            binders.putLocal("dDocAccount", "");
            binders.putLocal("xMOCNumber", MOCNo);
            binders.putLocal("xDocNumber", area + "-" + platform + "-" + disciplineCode + "-" + documentType + "-" + runningNumber);
        } else if (typeDrawing.equals(uploadTempHeaderMasterlistNonDrawing)) {
            binders.putLocal("xNdLocation", area);
            binders.putLocal("xNdPlatform", platform);
            binders.putLocal("xDocNumber", documentNumber);
            binders.putLocal("xNdDisciplineCode", disciplineCode);
            binders.putLocal("xNdDocTypeCode", documentType);
            binders.putLocal("xRunningNumber", runningNumber);
            binders.putLocal("xSheetNumber", sheetNumber);
            binders.putLocal("dDocTitle", documentTitle);
            binders.putLocal("xOldDocNumber", oldDocNo);
            binders.putLocal("xProjectName", projectName);
            binders.putLocal("xProjectYear", projectYear);
            binders.putLocal("xAuthorOrganization", ownerOrganization);
            binders.putLocal("xAuthor", ownerName);
            binders.putLocal("xAuthor", author);
            binders.putLocal("xRemarks", remarks);
            binders.putLocal("dDocAccount", "");
            binders.putLocal("xStatus", "Registered");
            binders.putLocal("xDocNumber", area + "-" + disciplineCode + "-" + documentType + "-" + runningNumber);
        } else if (typeDrawing.equals(uploadTempHeaderGeneral)) {
        	//Users users = new Users();
            binders.putLocal("xDocName", documentName);
            binders.putLocal("xDocNumber", documentNumber);
            binders.putLocal("xAuthor", author);
            binders.putLocal("xAuthorOrganization", authorOrganization);
            binders.putLocal("xProjectName", projectName);
            binders.putLocal("xCompany", company);
            binders.putLocal("xOldDocNumber", oldDocNo);
            binders.putLocal("xRetentionPeriod", retentionPeriod);
            binders.putLocal("xBUAssetFunction", buAssetFunction);
            binders.putLocal("dDocTitle", documentTitle);
            binders.putLocal("dDocAccount", "");
            binders.putLocal("destinationPath", destinationPath);
            binders.putLocal("xDocPurpose", "Published");
            binders.putLocal("xStatus", "Published");
            binders.putLocal("dDocType", documentType);
            binders.putLocal("xDocModel", docModel);
            binders.putLocal("xComments", description);
        } else if (typeDrawing.equals(uploadTempHeaderPublic)) {
        	//Users users = new Users();
            binders.putLocal("xDocName", documentName);
            binders.putLocal("xDocNumber", documentNumber);
            binders.putLocal("xAuthor", author);
            binders.putLocal("xAuthorOrganization", authorOrganization);
            binders.putLocal("xProjectName", projectName);
            binders.putLocal("xCompany", company);
            binders.putLocal("xOldDocNumber", oldDocNo);
            binders.putLocal("xRetentionPeriod", retentionPeriod);
            binders.putLocal("xBUAssetFunction", buAssetFunction);
            binders.putLocal("dDocTitle", documentTitle);
            binders.putLocal("dDocAccount", "");
            binders.putLocal("destinationPath", destinationPath);
            binders.putLocal("xDocPurpose", "Published");
            binders.putLocal("xStatus", "Published");
            binders.putLocal("dDocType", documentType);
            binders.putLocal("xDocModel", docModel);
            binders.putLocal("xComments", description);
        }  else if (typeDrawing.equals(uploadTempHeaderDrawing)) {
            binders.putLocal("xDLocation", area);
            binders.putLocal("xDPlatform", platform);
            binders.putLocal("xDDisciplineCode", disciplineCode);
            binders.putLocal("xDDocTypeCode", documentType);
            try {
                if (runningNumber.indexOf(" ") >= 0) {
                    runningNumber = runningNumber.substring(0, runningNumber.indexOf(" "));
                }
            } catch (Exception ex) {
            }
            binders.putLocal("xRunningNumber", runningNumber);
            binders.putLocal("xDocName", documentName);
            binders.putLocal("xDocNumber", documentNumber);
            binders.putLocal("dDocTitle", documentTitle);
            binders.putLocal("xDocPurpose", documentPurpose);
            binders.putLocal("xStatus", status);
            binders.putLocal("xMOCNumber", MOCNo);
            binders.putLocal("xOldDocNumber", oldDocNo);
            binders.putLocal("xAuthor", author);
            binders.putLocal("xAuthorOrganization", authorOrganization);
            binders.putLocal("xAuthorGroupName", authorGroupName);
            binders.putLocal("xCompany", company);
            binders.putLocal("xProjectName", projectName);
            binders.putLocal("xContractor", contractor);
            binders.putLocal("xContracNumber", contractNo);
            binders.putLocal("xWONumber", WONo);
            binders.putLocal("xDocModel", docModel);
            binders.putLocal("xComments", description);
            binders.putLocal("xRevision", revision);
            binders.putLocal("dDocAccount", "");
            binders.putLocal("xSheetNumber", sheetNumber);
            binders.putLocal("destinationPath", destinationPath);
        } else if (typeDrawing.equals(uploadTempHeaderNonDrawing)) {
            binders.putLocal("xNdLocation", area);
            binders.putLocal("xNdDisciplineCode", disciplineCode);
            binders.putLocal("xNdDocTypeCode", documentType);
            try {
                if (runningNumber.indexOf(" ") >= 0) {
                    runningNumber = runningNumber.substring(0, runningNumber.indexOf(" "));
                }
            } catch (Exception ex) {
            }
            binders.putLocal("xRunningNumber", runningNumber);
            binders.putLocal("xDocName", documentName);
            binders.putLocal("xDocNumber", documentNumber);
            binders.putLocal("dDocTitle", documentTitle);
            binders.putLocal("xDocPurpose", documentPurpose);
            binders.putLocal("xStatus", status);
            binders.putLocal("xOldDocNumber", oldDocNo);
            binders.putLocal("xAuthor", author);
            binders.putLocal("xAuthorOrganization", authorOrganization);
            binders.putLocal("xAuthorGroupName", authorGroupName);
            binders.putLocal("xCompany", company);
            binders.putLocal("xProjectName", projectName);
            binders.putLocal("xContractor", contractor);
            binders.putLocal("xContracNumber", contractNo);
            binders.putLocal("xWONumber", WONo);
            binders.putLocal("xRevision", revision);
            binders.putLocal("dDocAccount", "");
            binders.putLocal("destinationPath", destinationPath);
            binders.putLocal("xRetentionPeriod", retentionPeriod);
            if (activePassive.equalsIgnoreCase("Active")) {
                //Date date = new Date();
                String tanggal = retentionDate.substring(0, retentionDate.indexOf("/"));
                String sisa = retentionDate.substring(retentionDate.indexOf("/") + 1);
                String bulan = sisa.substring(0, sisa.indexOf("/"));
                sisa = sisa.substring(retentionDate.indexOf("/") + 1);
                String tahun = sisa;
                retentionDate = bulan + "/" + tanggal + "/" + tahun;
                DateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm a");
                try {
                    binders.putLocal("xRetentionDate", sdf.format(sdf.parse(retentionDate + " 11:59 PM")));
                    binder.setFieldType("xRetentionDate", "Date");
                } catch (Exception ex) {
                    Log.error("PHE:parsingDate:ex" + ex.getMessage());
                }
            }
        }
        //DateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");
        //Date date = new Date();
        try {
            if (runningNumber.indexOf(" ") >= 0) {
                runningNumber = runningNumber.substring(0, runningNumber.indexOf(" "));
            }
        } catch (Exception ex) {
        }
        binders.putLocal("dDocAuthor", userLogin);
        binders.putLocal("dDocType", docClasification);
        binders.putLocal("isCustom", "true");
        try {
            returnFunction = dColl.executeService(binders, userLogin, true);
            if (returnFunction.length() == 0) {
                returnFunction = binders.getLocal("dID") + ";" + binders.getLocal("dDocName");
            }
        } catch (Exception ex) {
            Log.error("PHE:executeFailed:indexRow:" + indexRow + ":ex:" + ex.getMessage().toString());
            return "executeFailed";
        }
        return returnFunction;
    }
    
    public String executeCheckinExtList(DataBinder binder, String dDocType, String dDocTitle, String xDocNumber, String xDepartment, String xExternalOwner, String xSubmitDate, String xYear, String xStampDate, String xComments, String xDocName, String xStatus, String Folder, String dDocAuthor, String xReferensi, String xAuthor, String dSecurityGroup, boolean isJavaMethod, String dDocName, int countJ, String xTypeNumber, String xBusinessStage)
    	    throws DataException, ServiceException {
    	    String returnFunction = "";
    	    DataCollection dColl = new DataCollection();
    	    //Validator validator = new Validator();
    	    DataBinder binders = new DataBinder();
    	    binders.putLocal("IdcService", "CHECKIN_UNIVERSAL");
    	    binders.putLocal("isJavaMethod", "true");
    	    if ((dDocName == "") || (dDocName == null)) {
    	      DateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");
    	      Date date = new Date();
    	      dDocName = "EXT" + dateFormat.format(date) + countJ;
    	    }
    	    
    	    binders.putLocal("dDocName", dDocName);
    	    binders.putLocal("dDocType", dDocType);
    	    binders.putLocal("dDocTitle", dDocTitle);
    	    binders.putLocal("xDocNumber", xDocNumber);
    	    binders.putLocal("xDepartment", xDepartment);
    	    binders.putLocal("xExternalOwner", xExternalOwner);
    	    binders.putLocal("xSubmitDate", xSubmitDate);
    	    binders.putLocal("xProjectYear", xYear);
    	    binders.putLocal("xStampDate", xStampDate);
    	    binders.putLocal("xComments", xComments);
    	    binders.putLocal("xDocName", xDocName);
    	    binders.putLocal("xStatus", xStatus);
    	    binders.putLocal("xTypeNumber", xTypeNumber);
    	    binders.putLocal("xBusinessStage", xBusinessStage);
    	    binders.putLocal("xReferensi", xReferensi);
    	    binders.putLocal("xAuthor", xAuthor);
    	    binders.putLocal("createPrimaryMetaFile", "1");
    	    binders.putLocal("dSecurityGroup", dSecurityGroup);
    	    binders.putLocal("dDocAuthor", dDocAuthor);
    	    binders.putLocal("isCustom", "true");
    	    binders.putLocal("dDocAccount", "");
    	    binders.putLocal("destinationPath", Folder);
    	    dDocAuthor = "weblogic";
    	    
    	    try {
    	      returnFunction = dColl.executeService(binders, dDocAuthor, true);
    	      if (returnFunction.length() == 0) {
    	        returnFunction = binders.getLocal("dID") + ";" + binders.getLocal("dDocName");
    	      }
    	    } catch (Exception ex) {
    	      Log.error("PHE:executeFailed:indexRow:");
    	      return "executeFailed";
    	    }
    	    return returnFunction;
    	  }
}