/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package intradocservice;

import intradoc.common.Log;
import intradoc.data.DataBinder;
import intradoc.data.ResultSet;
import intradoc.data.Workspace;
import intradoc.server.Service;
import util.DataCollection;

/**
 *
 * @author Ginanjar
 */
public class Users extends Service {

    public ResultSet getUserCredentials(
            String userLogin, String resultReturn) {
        DataCollection dColl = new DataCollection();

        DataBinder binders = new DataBinder();
        binders.putLocal("IdcService", "CHECK_USER_CREDENTIALS");

        binders.putLocal("dUser", userLogin);

        try {
            dColl.executeService(binders, userLogin, true, "");

            ResultSet rs = binders.getResultSet(resultReturn);

            Log.info("PHE:getQueryUserAttributes:" + resultReturn + ":" + rs);

            return rs;
        } catch (Exception ex) {
            Log.error("PHE:executeFailed:ex:" + ex.getMessage().toString());

            return null;
        }
    }

    public String getRoleByUser(String userLogin) {
        ResultSet rs = getUserCredentials(userLogin, "UserAttribInfo");
        String roles = "";

        //getRoles
        while (rs.next()) {
            if (userLogin.toUpperCase().equals(rs.getStringValue(0).toUpperCase())) {
                roles = rs.getStringValue(1);
            }
        }
        return roles;
    }
}
