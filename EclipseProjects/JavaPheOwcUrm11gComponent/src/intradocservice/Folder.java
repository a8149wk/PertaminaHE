package intradocservice;

import data.DatabaseValue;
import intradoc.common.Log;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.ResultSet;
import intradoc.data.Workspace;
import intradoc.server.Service;

public class Folder extends Service {
	public boolean DoCheckingFolder(String parentDir, String name, DataBinder binder, Workspace ws, Util util) {
		Log.info("Start Separate doc number");
		String[] names = name.split("/");

		int i = 0;
		for (String nama : names) {
			i++;
			if (!isFolderExist(parentDir + "/" + nama, binder, ws, util)) {
				if (CreateFolder(parentDir, nama, binder, ws, util)) {
					Log.info("CREATE FOLDER: " + nama);
				} else {
					Log.error("CREATE FOLDER failed: " + nama);
					return false;
				}
			}
			parentDir = parentDir + "/" + nama;
		}
		return true;
	}

	public boolean isFolderExist(String folderDir, DataBinder binder, Workspace ws, Util util) {
		boolean isExist = false;
		try {
			ResultSet rs = GetFolderInfo(folderDir, binder, ws, util);
			if (rs != null) {
				isExist = true;
			} else {
				isExist = false;
			}
		} catch (Exception e) {
			Log.error(e.getMessage());
		}
		return isExist;
	}

	public boolean CreateFolder(String parentDir, String folderName, DataBinder binder, Workspace ws, Util util) {
		try {
			ResultSet rs = GetFolderInfo(parentDir, binder, ws, util);
			if (rs != null) {
				rs.first();
				String folderGUID = binder.getResultSetValue(rs, "fFolderGUID");

				String serviceName = "FLD_CREATE_FOLDER";
				Log.info("parent :" + parentDir);
				Log.info("folder Name :" + folderName);
				Log.info("folder GUID :" + folderGUID);
				binder.putLocal("fParentGUID", folderGUID);
				binder.putLocal("IdcService", serviceName);
				binder.putLocal("fFolderName", folderName);
				binder.putLocal("fFolderType", "owner");
				Log.info("binder create folder :" + binder);

				util.executeService(binder, util.getUserLogin(), false, ws);
			} else {
				return false;
			}
		} catch (DataException e) {
			Log.error(e.getMessage());
			e.printStackTrace();
			return false;
		} catch (ServiceException e) {
			Log.error(e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public ResultSet GetFolderInfo(String path, DataBinder binder, Workspace ws, Util util) {
		try {
			Log.info("Getting folder information : " + path);
			binder.putLocal("path", path);
			String serviceName = "FLD_INFO";
			binder.putLocal("IdcService", serviceName);
			util.executeService(binder, util.getUserLogin(), false, ws);
			return binder.getResultSet("FolderInfo");
		} catch (DataException e) {
			Log.error(e.getMessage());
			return null;
		} catch (ServiceException e) {
			Log.error(e.getMessage());
		}
		return null;
	}

	public boolean IsContentExist(Workspace ws, String xlocation, String xplatform, String xdisciplincode,
			String xdoctype, String xrunningNumber, String xsheetNumber, boolean isTDCDrawing) {
		String query;
		if (isTDCDrawing) {
			if (xsheetNumber.length() == 0) {
				query = "select count(1)TOTAL from docmeta where xdlocation = '" + xlocation + "' and xdplatform = '"
						+ xplatform + "' and xddisciplinecode = '" + xdisciplincode + "' and xddoctypecode = '"
						+ xdoctype + "' and xrunningnumber = '" + xrunningNumber + "' and xSheetNumber is null ";
			} else {
				query = "select count(1)TOTAL from docmeta where xdlocation = '" + xlocation + "' and xdplatform = '"
						+ xplatform + "' and xddisciplinecode = '" + xdisciplincode + "' and xddoctypecode = '"
						+ xdoctype + "' and xrunningnumber = '" + xrunningNumber + "' and xSheetNumber = '"
						+ xsheetNumber + "'";
			}
		} else {
			query = "select count(1)TOTAL from docmeta where xndlocation = '" + xlocation
					+ "' and xnddisciplinecode = '" + xdisciplincode + "' and xnddoctypecode = '" + xdoctype
					+ "' and xrunningnumber = '" + xrunningNumber + "'";
		}
		Log.info("Query : " + query);
		try {
			ResultSet rs = ws.createResultSetSQL(query);
			if (!rs.isEmpty()) {
				rs.first();
				String total = rs.getStringValue(0);
				Log.info("Total : " + total);
				if (total.equalsIgnoreCase("1")) {
					return true;
				}
				if (total.equalsIgnoreCase("0")) {
					return false;
				}
			} else {
				return false;
			}
		} catch (DataException e) {
			Log.error(e.getMessage());
			return false;
		}
		return false;
	}

	public String GetDisCodeDesc(String type, String disCode, Workspace ws) {
		DatabaseValue db = new DatabaseValue();
		String table;
		if (type.toUpperCase().contains("NONDRAWING")) {
			table = "PHE_ND_DISCIPLINE_CODE";
		} else {
			table = "PHE_D_DISCIPLINE_CODE";
		}
		String query = "SELECT DESCRIPTION FROM " + table + " WHERE CODE LIKE '" + disCode + "'";
		String discCodeDesc = db.GetResultQuery(query, ws);
		return discCodeDesc == null ? "UNDEFINED" : discCodeDesc;
	}
}
