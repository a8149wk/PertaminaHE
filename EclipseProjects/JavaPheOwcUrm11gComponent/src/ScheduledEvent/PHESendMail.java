/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ScheduledEvent;

import data.DatabaseValue;
import intradoc.common.Log;
import intradoc.server.Service;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NandaY
 */
public class PHESendMail extends Service{
     public void pheSendMail() {
        
      String to      = m_binder.getLocal("pheMailTo");
      String message = m_binder.getLocal("pheMailMessage");
      String subject = m_binder.getLocal("pheMailSubject");
        DatabaseValue dValue = new DatabaseValue();
        String emailFrom = "";
        try {

            String usernameAuthenticator = dValue.getPheConfig("TDC_EMAIL_USERNAME");
            String passwordAuthenticator = dValue.getPheConfig("TDC_EMAIL_PASSWORD");
            String smtpHostName = dValue.getPheConfig("SMTP_EMAIL");
            Log.info("sethostname:"+smtpHostName);

            emailFrom = dValue.getPheConfig("TDC_EMAIL");
            HtmlEmail email = new HtmlEmail();
            email.setHostName(smtpHostName);
            email.setSmtpPort(25);
            email.setAuthenticator(new DefaultAuthenticator(usernameAuthenticator, passwordAuthenticator));
            email.setTLS(false);
            //email.setSSLOnConnect(true);
            email.setFrom(emailFrom);
            email.setSubject(subject);
            email.setHtmlMsg(message);

            //edit nanda 1009 - ilangin cc
//            if (cc.length() > 0) {
//                email.addCc(cc);
//            }


            email.setMsg(message);
            email.setTextMsg(message);
            String toSplit[] = to.split(",");
//            Log.info("PHE:--a:sendMail:to:" + to);
//            Log.info("PHE:--a:sendMail:toSplit.length:" + toSplit.length);

            for (int i = 0; i < toSplit.length; i++) {
                //added by nanda - 180614
                //cek dapat alamat email ato ngga
                if (toSplit[i].length() > 0) {
                    email.addTo(toSplit[i]);
                }
            }
            email.send();
            dValue.insertEmailLog(emailFrom, to, subject, "Success");
        } catch (Exception e) {
            Log.error("PHE:--a:sendMail:ex:" + e.getMessage());
            dValue.insertEmailLog(emailFrom, to, subject, "Failed");
            e.printStackTrace();
        }
    }
     
	public void pheSendMailWithCC() {
		String to = m_binder.getLocal("pheMailTo");
		String cc = m_binder.getLocal("pheMailCc");
		String message = m_binder.getLocal("pheMailMessage");
		String subject = m_binder.getLocal("pheMailSubject");
		DatabaseValue dValue = new DatabaseValue();
		String emailFrom = "";
		try {
			String usernameAuthenticator = dValue.getPheConfig("TDC_EMAIL_USERNAME");
			String passwordAuthenticator = dValue.getPheConfig("TDC_EMAIL_PASSWORD");
			String smtpHostName = dValue.getPheConfig("SMTP_EMAIL");
			Log.info("sethostname:" + smtpHostName);

			emailFrom = dValue.getPheConfig("TDC_EMAIL");
			HtmlEmail email = new HtmlEmail();
			email.setHostName(smtpHostName);
			email.setSmtpPort(25);
			email.setAuthenticator(new DefaultAuthenticator(usernameAuthenticator, passwordAuthenticator));
			email.setTLS(false);
			// email.setSSLOnConnect(true);
			email.setFrom(emailFrom);
			email.setSubject(subject);
			email.setHtmlMsg(message);

			email.setMsg(message);
			email.setTextMsg(message);
			String toSplit[] = to.split(",");
			String ccSplit[] = cc.split(",");

			for (int i = 0; i < toSplit.length; i++) {
				if (toSplit[i].length() > 0) {
					email.addTo(toSplit[i]);
				}
			}
			if (cc.length() > 0) {
				 for (int i = 0; i < ccSplit.length; i++) {
						if (ccSplit[i].length() > 0) {
							email.addCc(ccSplit[i]);
						}
					}
			}
			

			email.send();
			dValue.insertEmailLog(emailFrom, to, subject, "Success");
		} catch (Exception e) {
			Log.error("PHE:--a:sendMail:ex:" + e.getMessage());
			dValue.insertEmailLog(emailFrom, to, subject, "Failed");
			e.printStackTrace();
		}
	}
}
